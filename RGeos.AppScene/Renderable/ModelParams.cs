﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.AppScene.Renderable
{
    public class RModelParams
    {
        public string FileName { get; set; }
        public double RotateX { get; set; }
        public double RotateY { get; set; }
        public double RotateZ { get; set; }
        public double ScaleX { get; set; }
        public double ScaleY { get; set; }
        public double ScaleZ { get; set; }

        public SlimDX.Vector3 Position { get; set; }

    }
}
