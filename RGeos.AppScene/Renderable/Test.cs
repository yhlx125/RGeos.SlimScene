﻿using System;
using System.Drawing;
using RGeos.SlimScene.Core;
using SlimDX;
using SlimDX.Direct3D9;
using CustomVertex;
using RGeos.AppScene.Renderable;

namespace RGeos.SlimScene.Renderable
{
    /// <summary>
    /// 定义网格顶点和索引缓冲
    /// </summary>
    public class BoxMesh : RenderableObject
    {
        private CustomVertex.PositionColored[] vertices;//定义网格顶点
        private short[] indices;//定义网格中三角形索引
        private int modelVertCount = 0;
        private int modelFaceCount = 0;
        private VertexBuffer vertBuffer;
        private IndexBuffer indBuffer;

        public BoxMesh(string name)
            : base(name)
        {
            this.isSelectable = true;
        }
        private void ComputeVertexs()
        {
            vertices = new CustomVertex.PositionColored[6];

            Vector3 pt = new Vector3();
            pt.X = 0;
            pt.Y = 0;
            pt.Z = 0;
            vertices[0].Position = pt;
            vertices[0].Color = Color.Red.ToArgb();

            Vector3 pt1 = new Vector3();
            pt1.X = 0;
            pt1.Y = 10;
            pt1.Z = 0;
            vertices[1].Position = pt1;
            vertices[1].Color = Color.Red.ToArgb();

            Vector3 pt2 = new Vector3();
            pt2.X = 0;
            pt2.Y = 10;
            pt2.Z = 10;
            vertices[2].Position = pt2;
            vertices[2].Color = Color.Red.ToArgb();

            Vector3 pt3 = new Vector3();
            pt3.X = 0;
            pt3.Y = 20;
            pt3.Z = 20;
            vertices[3].Position = pt3;
            vertices[3].Color = Color.Blue.ToArgb();

            Vector3 pt4 = new Vector3();
            pt4.X = 0;
            pt4.Y = 20;
            pt4.Z = 0;
            vertices[4].Position = pt4;
            vertices[4].Color = Color.Blue.ToArgb();

            Vector3 pt5 = new Vector3();
            pt5.X = 20;
            pt5.Y = 0;
            pt5.Z = 0;
            vertices[5].Position = pt5;
            vertices[5].Color = Color.Blue.ToArgb();
        }

        /// <summary>
        /// 计算索引
        /// </summary>
        private void ComputeIndices()
        {
            indices = new short[6];

            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 3;
            indices[4] = 4;
            indices[5] = 5;

        }

        #region Renderable
        /// <summary>
        /// 初始化对象
        /// </summary>
        /// <param name="drawArgs">渲染参数</param>
        public override void Initialize(DrawArgs drawArgs)
        {
            LoadTexturesAndMaterials(drawArgs);//导入贴图和材质 
            ComputeVertexs();//计算顶点
            ComputeIndices();//计算索引

            vertBuffer = BufferCreator.CreateVertexBuffer(drawArgs.Device, vertices);
            indBuffer = BufferCreator.CreateIndexBuffer(drawArgs.Device, indices);
            modelVertCount = vertices.Length;
            modelFaceCount = indices.Length / 3;
            this.isInitialized = true;
        }


        /// <summary>
        /// 渲染对象
        /// </summary>
        /// <param name="drawArgs">渲染参数</param>
        public override void Render(DrawArgs drawArgs)
        {
            if (!this.IsOn || !this.isInitialized) return;
            //获取当前世界变换
            Matrix world = drawArgs.Device.GetTransform(TransformState.World);
            //获取当前顶点格式
            VertexFormat format = drawArgs.Device.VertexFormat;
            //获取当前的Z缓冲方式
            int zEnable = drawArgs.Device.GetRenderState(RenderState.ZEnable);
            //获取纹理状态
            int colorOper = drawArgs.Device.GetTextureStageState(0, TextureStage.ColorOperation);

            try
            {
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.Modulate);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Texture);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorArg2, TextureArgument.Diffuse);
                drawArgs.Device.SetTextureStageState(0, TextureStage.AlphaOperation, TextureOperation.Disable);

                //设置顶点格式
                drawArgs.Device.VertexFormat = CustomVertex.PositionColored.Format;
                //设置Z缓冲
                drawArgs.Device.SetRenderState(RenderState.ZEnable, 1);
                //设置纹理状态，此处使用纹理
                //drawArgs.Device.SetTexture(0, texture);//设置贴图 
                drawArgs.Device.SetStreamSource(0, vertBuffer, 0, PositionColored.SizeBytes);
                drawArgs.Device.Indices = indBuffer;
                drawArgs.Device.VertexFormat = PositionColored.Format;
                drawArgs.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, modelVertCount, 0, modelFaceCount);
            }
            catch (Exception e)
            {
                Utility.Log.Write(e);
            }
            finally
            {
                drawArgs.Device.SetTransform(TransformState.World, world);
                drawArgs.Device.VertexFormat = format;
                drawArgs.Device.SetRenderState(RenderState.ZEnable, zEnable);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, colorOper);
            }
            if (disposing)
            {
                Dispose();
                disposing = false;
            }
        }
        public bool disposing = false;
        /// <summary>
        /// 更新对象
        /// </summary>
        /// <param name="drawArgs">渲染参数</param>
        public override void Update(DrawArgs drawArgs)
        {
            if (!this.isInitialized)
            {
                this.Initialize(drawArgs);
            }
        }

        /// <summary>
        /// 执行选择操作
        /// </summary>
        /// <param name="X">点选X坐标</param>
        /// <param name="Y">点选Y坐标</param>
        /// <param name="drawArgs">渲染参数</param>
        /// <returns>选择返回True,否则返回False</returns>
        public bool PerformSelectionAction(int X, int Y, DrawArgs drawArgs)
        {
            return false;
        }


        /// <summary>
        /// 释放对象
        /// </summary>
        public override void Dispose()
        {
            this.isInitialized = false;
            //base.Dispose();
        }
        #endregion

        private void LoadTexturesAndMaterials(DrawArgs drawArgs)//导入贴图和材质 
        {

        }


        public override bool PerformSelectionAction(DrawArgs drawArgs)
        {
            bool flag = PerformSelectionAction(DrawArgs.LastMousePosition.X, DrawArgs.LastMousePosition.Y, drawArgs);
            return flag;
        }

    }
}

