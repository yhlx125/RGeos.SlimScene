﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.Direct3D9;
using CustomVertex;
using SlimDX;

namespace RGeos.AppScene.Renderable
{
    class BufferCreator
    {
        // 摘要:
        //     由顶点索引数组创建顶点索引缓存
        //
        // 参数:
        //   device:
        //     设备
        //
        //   indicesData:
        //     顶点索引数组
        //
        // 返回结果:
        //     顶点索引缓存
        public static IndexBuffer CreateIndexBuffer(Device device, int[] indicesData)
        {
            IndexBuffer indexBuffer = new IndexBuffer(device, 32 * indicesData.Length, Usage.WriteOnly, Pool.Default, true);
            DataStream ds = indexBuffer.Lock(0, 32 * indicesData.Length, LockFlags.None);
            ds.WriteRange(indicesData);
            indexBuffer.Unlock();
            ds.Dispose();
            return indexBuffer;
        }
        //
        // 摘要:
        //     由顶点索引数组创建顶点索引缓存
        //
        // 参数:
        //   device:
        //     设备
        //
        //   indicesData:
        //     顶点索引数组
        //
        // 返回结果:
        //     顶点索引缓存
        public static IndexBuffer CreateIndexBuffer(Device device, short[] indicesData)
        {
            IndexBuffer indexBuffer = new IndexBuffer(device, 16 * indicesData.Length, Usage.WriteOnly, Pool.Default, true);
            DataStream ds = indexBuffer.Lock(0, 16 * indicesData.Length, LockFlags.None);
            ds.WriteRange(indicesData);
            indexBuffer.Unlock();
            ds.Dispose();
            return indexBuffer;
        }
        //
        // 摘要:
        //     构建IndexBuffer
        //
        // 参数:
        //   device:
        //     设备
        //
        //   indicesData:
        //     顶点索引数组
        //
        // 类型参数:
        //   T:
        //     可以为short或int
        //
        // 返回结果:
        //     顶点索引缓存
        public static IndexBuffer CreateIndexBuffer<T>(Device device, T[] indicesData) where T : struct
        {
            return null;
        }

        /// <summary>
        /// 创建顶点缓冲区
        /// </summary>
        /// <param name="device">设备</param>
        /// <param name="vertices">顶点数组</param>
        /// <returns>顶点缓冲区</returns>
        public static VertexBuffer CreateVertexBuffer(Device device, PositionColored[] vertices)
        {
            VertexBuffer vertexBuffer = new VertexBuffer(device, vertices.Length * CustomVertex.PositionColored.SizeBytes, Usage.Dynamic | Usage.WriteOnly, CustomVertex.PositionColored.Format, Pool.Default);
            DataStream vs = vertexBuffer.Lock(0, vertices.Length * CustomVertex.PositionColored.SizeBytes, LockFlags.None);
            vs.WriteRange(vertices);
            vertexBuffer.Unlock();
            vs.Dispose();
            return vertexBuffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="vertices"></param>
        /// <returns></returns>
        public static VertexBuffer CreateVertexBuffer(Device device, PositionColoredTextured[] vertices)
        {
            VertexBuffer vertexBuffer = new VertexBuffer(device, vertices.Length * CustomVertex.PositionColoredTextured.SizeBytes, Usage.Dynamic | Usage.WriteOnly, CustomVertex.PositionColoredTextured.Format, Pool.Default);
            DataStream vs = vertexBuffer.Lock(0, vertices.Length * CustomVertex.PositionColoredTextured.SizeBytes, LockFlags.None);
            vs.WriteRange(vertices);
            vertexBuffer.Unlock();
            vs.Dispose();
            return vertexBuffer;
        }
        //
        // 摘要:
        //     创建顶点缓冲区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        // 返回结果:
        //     顶点缓冲区
        public static VertexBuffer CreateVertexBuffer(Device device, PositionNormal[] vertices)
        {
            return null;
        }
        //
        // 摘要:
        //     创建顶点缓冲区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        // 返回结果:
        //     顶点缓冲区
        public static VertexBuffer CreateVertexBuffer(Device device, PositionNormalColored[] vertices)
        {
            return null;
        }

        //
        // 摘要:
        //     创建顶点缓冲区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        // 返回结果:
        //     顶点缓冲区
        public static VertexBuffer CreateVertexBuffer(Device device, PositionTextured[] vertices)
        {
            return null;
        }
        //
        // 摘要:
        //     创建顶点缓冲区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        // 返回结果:
        //     顶点缓冲区
        public static VertexBuffer CreateVertexBuffer(Device device, TransformedTextured[] vertices)
        {
            return null;
        }
        //
        // 摘要:
        //     创建顶点缓冲区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        //   usage:
        //     用途
        //
        //   lockFlags:
        //     锁定标志
        //
        // 返回结果:
        //     顶点缓冲区
        public static VertexBuffer CreateVertexBuffer(Device device, PositionColored[] vertices, Usage usage, LockFlags lockFlags)
        {
            return null;
        }
        //
        // 摘要:
        //     创建顶点缓存区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        //   usage:
        //     用途
        //
        //   lockFlags:
        //     锁定标志
        //
        // 返回结果:
        //     顶点缓存区
        public static VertexBuffer CreateVertexBuffer(Device device, PositionColoredTextured[] vertices, Usage usage, LockFlags lockFlags)
        {
            return null;
        }

        public static VertexBuffer CreateVertexBuffer(Device device, PositionTextured[] vertices, Usage usage, LockFlags lockFlags)
        {
            return null;
        }

        public static VertexBuffer CreateVertexBuffer<T>(Device device, T[] vertices, Usage usage, LockFlags lockFlags) where T : struct
        {
            return null;
        }
        //
        //
        // 参数:
        //   device:
        //
        //   vertices:
        //
        //   usage:
        //
        //   lockFlags:
        public static VertexBuffer CreateVertexBuffer(Device device, TransformedColored[] vertices, Usage usage, LockFlags lockFlags)
        {
            return null;
        }
        //
        // 摘要:
        //     创建顶点缓冲区
        //
        // 参数:
        //   device:
        //     设备
        //
        //   vertices:
        //     顶点数组
        //
        //   usage:
        //     用途
        //
        //   lockFlags:
        //     锁定标志
        //
        // 返回结果:
        //     顶点缓冲区
        public static VertexBuffer CreateVertexBuffer(Device device, TransformedTextured[] vertices, Usage usage, LockFlags lockFlags)
        {
            return null;
        }
    }
}
