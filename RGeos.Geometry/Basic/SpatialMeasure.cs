﻿using System;
using SlimDX;
using RGeos.Geometry;

namespace RGeos.Basic
{
    public class SpatialMeasure
    {
        /// <summary>
        /// 两点间直线距离
        /// </summary>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <returns></returns>
        public static double Distance2D(Point3d preCoord, Point3d nextCoord)
        {
            return Math.Sqrt(Math.Pow((nextCoord.X - preCoord.X), 2) + Math.Pow((nextCoord.Y - preCoord.Y), 2));
        }


    }
}
