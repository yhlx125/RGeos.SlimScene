﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.SlimScene.Core;
using SlimDX;
using RGeos.Basic;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 架空线路
    /// </summary>
    public class OTLPolyline : List<OtlTowerPoint>, IRenderable
    {
        public RgLoopTypes mLoopNum { get; set; }
        //起点塔，在拓扑数据集中赋值
        public OtlTowerTopologicPoint TopologicStart { get; set; }
        //终点塔，在拓扑数据集中赋值
        public OtlTowerTopologicPoint TopologicEnd { get; set; }

        //单看线路的起点塔
        OtlTowerPoint mStart = null;
        public OtlTowerPoint Start
        {
            get
            {
                return mStart;
            }
        }

        //单看线路的终点塔
        OtlTowerPoint mEnd = null;
        public OtlTowerPoint End
        {
            get
            {
                return mEnd;
            }
        }

        IWireStyleSet mWireStyleSet = null;

        public IWireStyleSet EWireStyleSet
        {
            get { return mWireStyleSet; }
            set
            {
                mWireStyleSet = value;
            }
        }

        //线路包含的档的集合信息
        private OtlDangList mDangs = null;

        //线路包含的耐张段的集合信息
        private OtlStrainSectionList mStrainSections = null;

        public OTLPolyline()
        {

        }

        //添加抽象塔位点，构建档位成功
        public void AddTowerPoint(OtlTowerPoint towerPt)
        {
            //if (towerPt is OtlTowerTopologicPoint)
            //{
            //    OtlTowerTopologicPoint topoTowerPt = towerPt as OtlTowerTopologicPoint;
            //    //具有拓扑作用的塔，添加所属的拓扑弧段
            //    topoTowerPt.AddOwnerOtlPolyline(this);
            //    //拓扑点信息冗余，拷贝存储在每条线路中
            //    OtlTowerTopologicPoint topoTowerPtClone = topoTowerPt.Clone() as OtlTowerTopologicPoint;
            //    this.Add(topoTowerPtClone);
            //}
            //else
            //{
            //       this.Add(towerPt);
            //}
            this.Add(towerPt);
            if (this.Count == 1)
            {
                towerPt.NextDang = null;
                towerPt.PreDang = null;
                mStart = towerPt;
                mEnd = towerPt;
            }
            else if (this.Count == 2)
            {
                mDangs = new OtlDangList();

                OtlDang dang = new OtlDang(this[0], this[1], this);

                mDangs.Add(dang);
                this[0].NextDang = dang;
                this[1].PreDang = dang;
                this[1].NextDang = null;
                mEnd = this[1];
            }
            else
            {
                OtlDang dang = new OtlDang(this[this.Count - 2], this[this.Count - 1], this);
                mDangs.Add(dang);
                this[this.Count - 2].NextDang = dang;
                this[this.Count - 1].PreDang = dang;
                this[this.Count - 1].NextDang = null;
                mEnd = this[this.Count - 1];
            }
        }

        //删除抽象塔位点，构建档位成功
        public void RemoveTowerPoint(OtlTowerPoint towerPt)
        {
            this.Remove(towerPt);
            //删除拓扑关系
        }

        /// <summary>
        /// 是否通过计算获取转角
        /// </summary>
        /// <param name="calculateCornerAngel"></param>
        public void InitializeStrainSection(bool calculateCornerAngel)
        {
            if (this.Count > 1)
            {
                mStrainSections = new OtlStrainSectionList();
                List<int> indxCornerAngle = new List<int>();
                indxCornerAngle.Add(0);
                for (int i = 1; i < this.Count; i++)
                {
                    if (calculateCornerAngel)
                    {
                        //计算转角
                        double angel = this[i].GetCornerAngle();
                        if (angel >= 0.001)
                        {
                            indxCornerAngle.Add(i);
                        }
                    }
                    else
                    {
                        if (this[i].CornerAngle != 0)
                        {
                            indxCornerAngle.Add(i);
                        }
                    }
                }
                indxCornerAngle.Add(this.Count - 1);
                for (int i = 0; i < indxCornerAngle.Count - 1; i++)
                {
                    //根据转角将档增加至耐张段
                    OtlStrainSection sect = new OtlStrainSection();
                    sect.InitialWireStyle(mWireStyleSet);
                    for (int j = indxCornerAngle[i]; j < indxCornerAngle[i + 1]; j++)
                    {
                        mDangs[j].OwnerStrainSection = sect;
                        sect.AddDang(mDangs[j]);
                    }
                    mStrainSections.Add(sect);
                }

            }
        }

        //回路编号，索引，标识属于第几回路，默认为单回路为0，双回路为0，1
        //该值和挂点中的回路索引保持一致
        public void SetLoopIndex(int indexClassify, int newIndex)
        {
            for (int i = 0; i < this.Count; i++)
            {
                OtlTowerPoint otlPt = this[i];
                if (otlPt != null)
                {
                    otlPt.HangPoints.SetLoopIndex(indexClassify, newIndex);
                }
            }
        }


        #region  IRenderable
        public bool IsOn = true;
        public bool IsInitialize = false;
        public void Initialize(DrawArgs drawArgs)
        {
            if (IsOn && !IsInitialize)
            {
                ////准备好对象
                //for (int i = 1; i < mDangs.Count - 1; i++)
                //{
                //    mDangs[i].Initialize(drawArgs);
                //}
                IsInitialize = true;
            }
        }

        public void Update(DrawArgs drawArgs)
        {
            if (IsOn && !IsInitialize)
            {
                Initialize(drawArgs);
            }
            //渲染线路
            for (int i = 1; i < mDangs.Count - 1; i++)
            {
                if (drawArgs.WorldCamera.ViewFrustum.Contains(mDangs[i].BoundingBox))
                {
                    if (!mDangs[i].IsInitialize)
                    {
                        mDangs[i].Initialize(drawArgs);
                    }
                    else
                    {
                        mDangs[i].Update(drawArgs);
                    }
                }
                else
                {
                    if (mDangs[i].IsInitialize)
                    {
                        mDangs[i].Dispose();
                    }
                }
            }
            for (int i = 0; i < this.Count; i++)
            {
                if (!this[i].IsInitialize)
                {
                    if (this[i].TowerSymbol != null)
                    {
                        Vector3 direction = this[i].GetForwardDirection();
                        double retotate = RgMath.GetQuadrantAngle(direction.X, direction.Y);
                        this[i].TowerSymbol.SetRotateZ(retotate + Math.PI / 2);
                        this[i].TowerSymbol.SetTransform(this[i].Position);
                    }
                    this[i].Initialize(drawArgs);
                }
                else
                {
                    this[i].Update(drawArgs);
                }
            }
        }

        public void Render(DrawArgs drawArgs)
        {
            if (IsOn && IsInitialize)
            {
                //渲染线路
                for (int i = 1; i < mDangs.Count - 1; i++)
                {
                    mDangs[i].Render(drawArgs);
                }
                for (int i = 0; i < this.Count; i++)
                {
                    this[i].Render(drawArgs);
                }
            }
        }

        public void Dispose()
        {

            IsInitialize = false;
        }
        #endregion

    }
}
