﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.SlimScene.Core;
using RGeos.Geometry;
using SlimDX;
using System.Drawing;
using RGeos.Basic;

namespace RGeos.Framework.OTL.Renderable
{
    //简单立塔
    public class SimpleTowerModel : ITowerSymbol
    {
        public bool IsOn = true;
        public bool IsInitialize = false;
        public string Name { get; set; }
        public BoundingBox BoundingBox;
        double WireHeight;
        double WireWidth;
        double WireGroundHeight;
        double WireGroundWidth;
        List<LineString> lineStrings = new List<LineString>();
        private List<RenderableLineString> listRenders = null;
        public SimpleTowerModel(double wireHeight, double wireWidth, double wireGroundHeight, double wireGroundWidth)
        {
            WireHeight = wireHeight;
            WireWidth = wireWidth;
            WireGroundHeight = wireGroundHeight;
            WireGroundWidth = wireGroundWidth;
            LineString line = new LineString();
            line.Color = Color.Yellow;
            Point3d[] pts = new Point3d[2];
            Point3d pt = new Point3d(0, 0, 0);
            Point3d pt2 = new Point3d(0, 0, WireGroundHeight);
            pts[0] = pt;
            pts[1] = pt2;
            line.Coordinates = pts;
            lineStrings.Add(line);

            LineString line2 = new LineString();
            line2.Color = Color.Yellow;
            Point3d[] pts2 = new Point3d[2];
            Point3d pt20 = new Point3d(-wireWidth, 0, WireHeight);
            Point3d pt22 = new Point3d(wireWidth, 0, WireHeight);
            pts2[0] = pt20;
            pts2[1] = pt22;
            line2.Coordinates = pts2;
            lineStrings.Add(line2);

            LineString line3 = new LineString();
            line3.Color = Color.Yellow;
            Point3d[] pts3 = new Point3d[2];
            Point3d pt30 = new Point3d(-wireGroundWidth, 0, WireGroundHeight);
            Point3d pt32 = new Point3d(wireGroundWidth, 0, WireGroundHeight);
            pts3[0] = pt30;
            pts3[1] = pt32;
            line3.Coordinates = pts3;
            lineStrings.Add(line3);
        }

        public void SetTransform(Vector3 position)
        {
            mPosition = position;
        }
        public double mAngle { get; set; }
        public Vector3 mPosition { get; set; }
        public void SetRotateZ(double angle)
        {
            mAngle = angle;
        }

        public void Initialize(DrawArgs drawArgs)
        {
            if (listRenders == null)
            {
                listRenders = new List<RenderableLineString>();
            }
            for (int i = 0; i < lineStrings.Count; i++)
            {

                LineString newline = new LineString();
                newline.Color = lineStrings[i].Color;
                Point3d[] pts3 = new Point3d[lineStrings[i].Coordinates.Length];
                for (int j = 0; j < lineStrings[i].Coordinates.Length; j++)
                {
                    Point3d pt0 = new Point3d();
                    if (lineStrings[i].Coordinates[j].X == 0 && lineStrings[i].Coordinates[j].Y == 0)
                    {
                        pt0.X = mPosition.X + 0;
                        pt0.Y = mPosition.Y + 0;
                        pt0.Z = lineStrings[i].Coordinates[j].Z;
                    }
                    else
                    {
                        double angel1 = RgMath.GetQuadrantAngle(lineStrings[i].Coordinates[j].X, lineStrings[i].Coordinates[j].Y);
                        double l = RgMath.d(new Vector3(0, 0, 0), new Vector3((float)lineStrings[i].Coordinates[j].X, (float)lineStrings[i].Coordinates[j].Y, 0));
                        double angleCoord = (mAngle + angel1) > 2 * Math.PI ? (mAngle + angel1) - 2 * Math.PI : (mAngle + angel1);
                        pt0.X = mPosition.X + l * Math.Cos(angleCoord);
                        pt0.Y = mPosition.Y + l * Math.Sin(angleCoord);
                        pt0.Z = lineStrings[i].Coordinates[j].Z;
                    }
                    pts3[j] = pt0;

                }
                newline.Coordinates = pts3;

                RenderableLineString render = new RenderableLineString(Name, drawArgs.CurrentWorld, newline.Coordinates, newline.Color);
                render.IsOn = true;
                render.RenderPriority = RenderPriority.Custom;
                listRenders.Add(render);
            }
            IsInitialize = true;
        }

        public void Update(DrawArgs drawArgs)
        {
            if (IsOn && !IsInitialize)
            {
                Initialize(drawArgs);
            }
            if (listRenders == null)
            {
                return;
            }
            for (int i = 0; i < listRenders.Count; i++)
            {
                listRenders[i].Initialize(drawArgs);
            }
        }

        public void Render(DrawArgs drawArgs)
        {
            if (IsOn && IsInitialize)
            {
                for (int i = 0; i < listRenders.Count; i++)
                {
                    if (listRenders[i].Initialized)
                    {
                        listRenders[i].Render(drawArgs);
                    }

                }
            }
        }

        public void Dispose()
        {
            if (listRenders != null)
            {
                listRenders.Clear();
            }
            if (lineStrings != null)
            {
                lineStrings.Clear();
            }
            IsInitialize = false;
        }
    }
}
