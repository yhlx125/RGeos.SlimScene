﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using RGeos.SlimScene.Core;
using SlimDX.Direct3D9;
using System.Drawing;
using RGeos.SlimScene.Renderable;
using System.IO;

namespace RGeos.Framework.OTL.Renderable
{
    public class Tower1 : ITowerSymbol
    {
        Mesh mesh = null;
        Texture[] meshTextures;
        Material[] meshMaterials1;
        ZModelParams mModelParams;
        public bool isInitialized;
        public bool isOn;
        public Tower1(ZModelParams modelParams)
        {
            mModelParams = modelParams;
        }
        public void Initialize(DrawArgs drawArgs)
        {
            //下句从tiger.x文件中读入3D图形(立体老虎)
            mesh = Mesh.FromFile(drawArgs.Device, mModelParams.FileName, MeshFlags.SystemMemory);
            string dir = Path.GetDirectoryName(mModelParams.FileName);
            ExtendedMaterial[] materials = mesh.GetMaterials();

            if (meshTextures == null)//如果还未设置纹理，为3D图形增加纹理和材质
            {
                meshTextures = new Texture[materials.Length];//纹理数组
                meshMaterials1 = new Material[materials.Length];//材质数组
                for (int i = 0; i < materials.Length; i++)//读入纹理和材质
                {
                    meshMaterials1[i] = materials[i].MaterialD3D;
                    meshMaterials1[i].Ambient = materials[i].MaterialD3D.Diffuse;

                    //meshMaterials1[i].Diffuse = Color.Brown;
                    meshTextures[i] = Texture.FromFile(drawArgs.Device, dir + @"\" + materials[i].TextureFileName);
                }
            }
            this.isInitialized = true;
        }

        public void Update(DrawArgs drawArgs)
        {
            if (!isInitialized && isOn)
            {
                Initialize(drawArgs);
            }
        }

        public void Render(DrawArgs drawArgs)
        {
            if (!this.isOn || !this.isInitialized) return;
            VertexFormat format = drawArgs.Device.VertexFormat;
            Matrix world = drawArgs.Device.GetTransform(TransformState.World);
            int currentCull = drawArgs.Device.GetRenderState(RenderState.FillMode);
            int currentColorOp = drawArgs.Device.GetTextureStageState(0, TextureStage.ColorOperation);
            int zBuffer = drawArgs.Device.GetRenderState(RenderState.ZEnable);
            try
            {
                drawArgs.Device.SetRenderState(RenderState.Lighting, true);
                drawArgs.Device.SetRenderState(RenderState.ZEnable, false);		 	//允许使用深度缓冲

                //drawArgs.Device.SetRenderState(RenderState.FillMode, FillMode.Solid);

                Light light = new Light();
                light.Type = LightType.Directional;
                light.Diffuse = Color.White;
                light.Ambient = Color.White;
                light.Direction = new Vector3(0, 0, -1);
                drawArgs.Device.SetLight(0, light);
                drawArgs.Device.EnableLight(0, true);

                //drawArgs.Device.Lights[0].Type= LightType.Directional; //设置灯光类型
                //drawArgs.Device.Lights[0].Diffuse = Color.White;			//设置灯光颜色
                //drawArgs.Device.Lights[0].Direction = new Vector3(0, -1, 0);	//设置灯光位置
                //drawArgs.Device.Lights[0].Update();						//更新灯光设置，创建第一盏灯光
                //drawArgs.Device.Lights[0].Enabled = true;					//使设置有效

                //drawArgs.Device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                //drawArgs.Device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                //drawArgs.Device.TextureState[0].ColorArgument2 = TextureArgument.Diffuse;
                //drawArgs.Device.TextureState[0].AlphaOperation = TextureOperation.Disable;
                drawArgs.Device.SetRenderState(RenderState.DiffuseMaterialSource, ColorSource.Material);
                drawArgs.Device.SetRenderState(RenderState.AlphaBlendEnable, true);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.Modulate);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Diffuse);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorArg2, TextureArgument.Texture);
                drawArgs.Device.SetTextureStageState(0, TextureStage.AlphaOperation, TextureOperation.Disable);
                drawArgs.Device.SetRenderState(RenderState.Ambient, Color.White.ToArgb());
                ;
                Matrix tmp = Matrix.Scaling(0.1f, 0.1f, 0.1f) * Matrix.RotationX((float)Math.PI / 2); ;
                tmp *= Matrix.RotationZ((float)mAngle);
                tmp = tmp * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
                drawArgs.Device.SetTransform(TransformState.World, tmp);
                for (int i = 0; i < meshMaterials1.Length; i++)//Mesh中可能有多个3D图形，逐一显示
                {
                    drawArgs.Device.Material = meshMaterials1[i];//设定3D图形的材质
                    drawArgs.Device.SetTexture(0, meshTextures[i]);//设定3D图形的纹理
                    mesh.DrawSubset(i);//显示该3D图形
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                drawArgs.Device.VertexFormat = format;
                drawArgs.Device.SetTransform(TransformState.World, world);
                drawArgs.Device.SetRenderState(RenderState.FillMode, currentCull);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, currentColorOp);
                drawArgs.Device.SetRenderState(RenderState.ZEnable, zBuffer);
                drawArgs.Device.SetRenderState(RenderState.Lighting, false);
                drawArgs.Device.EnableLight(0, false);
            }

        }

        public void Dispose()
        {
            if (mesh != null)
            {
                mesh.Dispose();
                mesh = null;
            }
            if (meshTextures != null)
            {

            }
        }

        public double mAngle
        {
            get;
            set;
        }

        public Vector3 mPosition
        {
            get;
            set;
        }

        public void SetTransform(Vector3 position)
        {
            mPosition = position;
        }

        public void SetRotateZ(double angle)
        {
            mAngle = angle;
        }
    }
}
