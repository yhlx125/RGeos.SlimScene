﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using RGeos.SlimScene.Core;
using SlimDX.Direct3D9;
using System.Drawing;

namespace RGeos.Framework.OTL.Renderable
{
    public class Axis : ITowerSymbol// IRenderable
    {
        public bool IsOn = true;
        public bool IsInitialize = false;
        public Mesh mMeshArrow = null;
        public Mesh mMeshStick = null;
        private Material material;//定义材质变量 
        private Material materialStick;//定义材质变量 
        public void Initialize(DrawArgs drawArgs)
        {
            if (IsOn && !IsInitialize)
            {
                mMeshArrow = Mesh.CreateCylinder(drawArgs.Device, 1.5f, 0.0f, 5.0f, 12, 5);
                mMeshStick = Mesh.CreateCylinder(drawArgs.Device, 0.5f, 0.5f, 10.0f, 6, 6);
                LoadTexturesAndMaterials(drawArgs);
            }
            IsInitialize = true;
        }
        private void LoadTexturesAndMaterials(DrawArgs drawArgs)//导入贴图和材质 
        {
            material = new Material();
            material.Diffuse = Color.Red;
            material.Ambient = Color.White;
            material.Specular = Color.Yellow;
            material.Power = 15.0F;

            materialStick = new Material();
            materialStick.Diffuse = Color.Yellow;
            materialStick.Ambient = Color.White;
            materialStick.Specular = Color.Yellow;
            materialStick.Power = 15.0F;
        }
        public void Update(DrawArgs drawArgs)
        {
            if (IsOn && !IsInitialize)
            {
                Initialize(drawArgs);
            }
        }

        public void Render(DrawArgs drawArgs)
        {
            Matrix world = drawArgs.Device.GetTransform(TransformState.World);
            int currentColorOp = drawArgs.Device.GetTextureStageState(0, TextureStage.ColorOperation);
            int lightCur = drawArgs.Device.GetRenderState(RenderState.Lighting);
            try
            {
                drawArgs.Device.SetRenderState(RenderState.Lighting, true);
                drawArgs.Device.EnableLight(0, true);
                AxisX(drawArgs);
                AxisY(drawArgs);
                AxisZ(drawArgs);

            }
            catch (Exception)
            {
            }
            finally
            {
                drawArgs.Device.EnableLight(0, false);
                drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, currentColorOp);
                drawArgs.Device.SetTransform(TransformState.World, world);
                drawArgs.Device.SetRenderState(RenderState.Lighting, lightCur);
            }

        }

        public void AxisX(DrawArgs drawArgs)
        {
            Light light = new Light();
            light.Type = LightType.Spot;
            light.Diffuse = Color.White;
            light.Direction = new Vector3(mPosition.X, mPosition.Y + 2, mPosition.Z + 4);
            drawArgs.Device.SetLight(0, light);

            //drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.SelectArg1);
            drawArgs.Device.SetRenderState(RenderState.Ambient, Color.FromArgb(Color.Red.ToArgb()).ToArgb());

            drawArgs.Device.Material = material;

            Matrix MoveModel = Matrix.Translation(new Vector3(12.5f, 0f, 0f));
            MoveModel = Matrix.RotationY((float)Math.PI / 2) * MoveModel;//右乘
            Matrix tmp = Matrix.RotationZ((float)mAngle);
            tmp = tmp * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
            tmp = MoveModel * tmp;
            drawArgs.Device.SetTransform(TransformState.World, tmp);
            //设置顶点格式
            mMeshArrow.DrawSubset(0);

            Light light2 = new Light();
            light2.Type = LightType.Directional;
            light2.Diffuse = Color.White;
            light.Direction = new Vector3(mPosition.X, mPosition.Y + 2, mPosition.Z + 4);
            drawArgs.Device.SetLight(0, light2);

            drawArgs.Device.SetRenderState(RenderState.Ambient, Color.FromArgb(Color.Red.ToArgb()).ToArgb());

            drawArgs.Device.Material = materialStick;

            Matrix MoveModel2 = Matrix.Translation(new Vector3(5, 0, 0));
            MoveModel2 = Matrix.RotationY((float)Math.PI / 2) * MoveModel2;
            Matrix tmp2 = Matrix.RotationZ((float)mAngle);
            tmp2 = tmp2 * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
            tmp2 = MoveModel2 * tmp2;
            drawArgs.Device.SetTransform(TransformState.World, tmp2);
            mMeshStick.DrawSubset(0);
        }

        public void AxisY(DrawArgs drawArgs)
        {
            
            Light light = new Light();
            light.Type = LightType.Spot;
            light.Diffuse = Color.White;
            light.Direction = new Vector3(mPosition.X, mPosition.Y + 2, mPosition.Z + 4);
            drawArgs.Device.SetLight(0, light);

            //drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.SelectArg1);
            drawArgs.Device.SetRenderState(RenderState.Ambient, Color.FromArgb(Color.Green.ToArgb()).ToArgb());

            drawArgs.Device.Material = material;

            Matrix MoveModel = Matrix.Translation(new Vector3(0f, 12.5f, 0f));
            MoveModel = Matrix.RotationX((float)-Math.PI / 2) * MoveModel;//右乘
            Matrix tmp = Matrix.RotationZ((float)mAngle);
            tmp = tmp * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
            tmp = MoveModel * tmp;
            drawArgs.Device.SetTransform(TransformState.World, tmp);
            //设置顶点格式
            mMeshArrow.DrawSubset(0);

            Light light2 = new Light();
            light2.Type = LightType.Directional;
            light2.Diffuse = Color.White;
            light.Direction = new Vector3(mPosition.X, mPosition.Y + 2, mPosition.Z + 4);
            drawArgs.Device.SetLight(0, light2);

            drawArgs.Device.SetRenderState(RenderState.Ambient, Color.FromArgb(Color.Green.ToArgb()).ToArgb());

            drawArgs.Device.Material = materialStick;

            Matrix MoveModel2 = Matrix.Translation(new Vector3(0, 5, 0));
            MoveModel2 = Matrix.RotationX((float)-Math.PI / 2) * MoveModel2;
            Matrix tmp2 = Matrix.RotationZ((float)mAngle);
            tmp2 = tmp2 * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
            tmp2 = MoveModel2 * tmp2;
            drawArgs.Device.SetTransform(TransformState.World, tmp2);
            mMeshStick.DrawSubset(0);
        }

        public void AxisZ(DrawArgs drawArgs)
        {
            
            Light light = new Light();
            light.Type = LightType.Spot;
            light.Diffuse = Color.White;
            light.Direction = new Vector3(mPosition.X, mPosition.Y + 2, mPosition.Z + 4);
            drawArgs.Device.SetLight(0, light);

            //drawArgs.Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.SelectArg1);
            drawArgs.Device.SetRenderState(RenderState.Ambient, Color.FromArgb(Color.Blue.ToArgb()).ToArgb());

            drawArgs.Device.Material = material;

            Matrix MoveModel = Matrix.Translation(new Vector3(0f, 0f, 12.5f));
          //  MoveModel = Matrix.RotationX((float)Math.PI) * MoveModel;//右乘
            Matrix tmp = Matrix.RotationZ((float)mAngle);
            tmp = tmp * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
            tmp = MoveModel * tmp;
            drawArgs.Device.SetTransform(TransformState.World, tmp);
            //设置顶点格式
            mMeshArrow.DrawSubset(0);

            Light light2 = new Light();
            light2.Type = LightType.Directional;
            light2.Diffuse = Color.White;
            light.Direction = new Vector3(mPosition.X, mPosition.Y + 2, mPosition.Z + 4);
            drawArgs.Device.SetLight(0, light2);

            drawArgs.Device.SetRenderState(RenderState.Ambient, Color.FromArgb(Color.Blue.ToArgb()).ToArgb());

            drawArgs.Device.Material = materialStick;

            Matrix MoveModel2 = Matrix.Translation(new Vector3(0, 0, 5));
           // MoveModel2 = Matrix.RotationX((float)Math.PI) * MoveModel2;
            Matrix tmp2 = Matrix.RotationZ((float)mAngle);
            tmp2 = tmp2 * Matrix.Translation(mPosition.X, mPosition.Y, mPosition.Z);
            tmp2 = MoveModel2 * tmp2;
            drawArgs.Device.SetTransform(TransformState.World, tmp2);
            mMeshStick.DrawSubset(0);
        }


        public void Dispose()
        {
            if (mMeshArrow != null && !mMeshArrow.Disposed)
            {
                mMeshArrow.Dispose();
                mMeshArrow = null;
            }
            if (mMeshStick != null && !mMeshStick.Disposed)
            {
                mMeshStick.Dispose();
                mMeshStick = null;
            }
            IsInitialize = false;
        }

        public double mAngle
        {
            get;
            set;
        }

        public Vector3 mPosition
        {
            get;
            set;
        }

        public void SetTransform(Vector3 position)
        {
            mPosition = position;
        }

        public void SetRotateZ(double angle)
        {
            mAngle = angle;
        }
    }
}
