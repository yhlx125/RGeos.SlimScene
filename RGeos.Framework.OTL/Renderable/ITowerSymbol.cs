﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.SlimScene.Core;
using SlimDX;

namespace RGeos.Framework.OTL.Renderable
{
    public interface ITowerSymbol : IRenderable
    {
        double mAngle { get; set; }
        Vector3 mPosition { get; set; }
        void SetTransform(Vector3 position);
        void SetRotateZ(double angle);
    }
}
