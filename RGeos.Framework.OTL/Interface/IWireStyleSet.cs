﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.Framework.OTL.OTLCalculate;

namespace RGeos.Framework.OTL
{
    public interface IWireStyleSet
    {
        HangWireTypeSet[] InitialWireStyle();
    }
}
