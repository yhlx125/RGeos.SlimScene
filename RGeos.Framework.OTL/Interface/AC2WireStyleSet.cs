﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.Interface
{
    public class AC2WireStyleSet : IWireStyleSet
    {
        public OTLCalculate.HangWireTypeSet[] InitialWireStyle()
        {
            OTLCalculate.HangWireTypeSet[] HangWireTypeSets = new OTLCalculate.HangWireTypeSet[8];
            HangWireTypeSets[0] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[1] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[2] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[3] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[4] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[5] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[6] = new OTLCalculate.HangWireTypeSet();
            HangWireTypeSets[7] = new OTLCalculate.HangWireTypeSet();
            return HangWireTypeSets;
        }
    }
}
