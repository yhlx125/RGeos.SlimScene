﻿
namespace RGeos.Framework.OTL.Interface
{
    /// <summary>
    /// 根据相的情况构建挂点
    /// </summary>
    public interface IOTLHangPointsFactory
    {
        IOTLHangPoints OtlHangPoints { get; set; }
        //构造挂点数组
        void CreateOTLHangPoints(RgLoopTypes loopType, RgHangPointTowerTypes towerHangPointType, RgTopoPointTypes topoPointType);
        //返回挂点的结构信息，存储在DataTable中？
        void GetHangPointsScheme();

        void InitialHangPointsValues(SlimDX.Vector3[] hangPoints);

        void InitialDefaultHangPointsValues();
    }
}
