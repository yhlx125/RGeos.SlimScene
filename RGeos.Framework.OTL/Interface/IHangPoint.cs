﻿using System;
using RGeos.Framework.OTL.OTLCalculate;
namespace RGeos.Framework.OTL.Interface
{
    /// <summary>
    /// 通过导线、地线、跳线区分挂点类型
    /// 通过大号侧小号侧区分前后
    /// 通过挂点索引区分如何构成抛物线
    /// </summary>
    public interface IHangPoint
    {
        //挂点编码，
        long Code { get; set; }

        //挂点名称
        string Name { get; set; }

        /************************
         * 挂点坐标
         * 以塔位中心点为原点，线路前视方向为y轴，建模的模型坐标系坐标值
         ***********************/
        SlimDX.Vector3 Coordinate { get; set; }



        /**************************************
        * 通过工厂构建的挂点默认通过枚举RgLoopIndexs赋值，起到归类的作用，构造线路以此为依据给LoopIndex赋值
        **************************************/
        int LoopIndexForClassify { get; set; }

        /**************************************
        * 所在回路的回路索引,第几回路，每个回路索引从0开始计
        * 该值需要在线路中赋值，没有确定线路的拓扑类型，无法为其赋值
        **************************************/
        int LoopIndex { get; set; }


        /*******************************
         * 用于区分导线挂点、地线挂点或者是跳线挂点
         * 从枚举RgHangPointWireTypes中获得值
         *******************************/
        int Type { get; set; }

        /******************************
         * 大号侧还是小号侧，从枚举RgIsForward中获得值
         ******************************/
        int Forward { get; set; }

        /**************************************
         * 挂点索引,
         **************************************/
        int PointIndex { get; set; }

        //是否也采用挂点矩阵的形式，如果采用这种形式，则产生2个挂点矩阵的集合导线挂点矩阵和地线挂点矩阵
        int PointRowIndex { get; set; }
        int PointColumnIndex { get; set; }

        IInsulator Insulator { get; set; }

        IHangPoint GetAssembleHangPoint();
    }
}
