﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.Interface
{
    /// <summary>
    /// 直流电
    /// </summary>
    public class DC2HangPointsFactory
    {
        IOTLHangPoints otlHangPoints = null;

        public IOTLHangPoints OtlHangPoints
        {
            get { return otlHangPoints; }
            set { otlHangPoints = value; }
        }
        public void CreateOTLHangPoints(RgLoopTypes loopType, RgHangPointTowerTypes towerHangPointType, RgTopoPointTypes topoPointType)
        {
            IOTLHangPoints otlHangPoints = new OTLHangPoints();
            otlHangPoints.LoopType = loopType;
            otlHangPoints.TopoPointType = topoPointType;
            otlHangPoints.TowerHangPointType = towerHangPointType;
            if (loopType == RgLoopTypes.One && towerHangPointType == RgHangPointTowerTypes.Z)
            {
                otlHangPoints.HangPoints = new HangPoint[5];
            }
            IHangPoint left = new HangPoint();
            left.Code = 0x0000001;
            left.Name = "左地线";
            IHangPoint right = new HangPoint();
            right.Code = 0x0000002;
            right.Name = "左地线";
        }

        public void GetHangPointsScheme()
        {

        }

        public void InitialHangPointsValues(SlimDX.Vector3[] hangPoints)
        {
            if (otlHangPoints == null)
            {
                throw new ArgumentException("没有定义挂点信息！");
            }
            if (hangPoints.Length < otlHangPoints.PointCount)
            {
                throw new ArgumentException("挂点数目不足！");
            }
        }

        public void InitialDefaultHangPointsValues()
        {
            if (otlHangPoints == null)
            {
                throw new ArgumentException("没有定义挂点信息！");
            }
        }
    }
}
