﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using RGeos.Framework.OTL.OTLCalculate;

namespace RGeos.Framework.OTL.Interface
{
    //单个挂点
    public class HangPoint : IHangPoint
    {
        public Vector3 Coordinate { get; set; }

        public string Name { get; set; }

        public long Code { get; set; }

        //导线、地线还是跳线
        public int Type { get; set; }

        //大号侧还是小号侧
        public int Forward { get; set; }

        //分类回路
        public int LoopIndexForClassify { get; set; }

        //所在回路,第几回路
        public int LoopIndex { get; set; }

        //挂点索引
        public int PointIndex { get; set; }

        //是否也采用挂点矩阵的形式
        public int PointRowIndex { get; set; }
        public int PointColumnIndex { get; set; }

        public IInsulator Insulator { get; set; }

        public IHangPoint GetAssembleHangPoint()
        {
            IHangPoint temp = new HangPoint();
            if (Insulator != null)
            {
                temp.Name = this.Name;
                temp.Code = this.Code;
                temp.Coordinate = this.Coordinate - Insulator.BlowHangPoint;
                temp.Forward = this.Forward;
                temp.LoopIndex = LoopIndex;
                temp.LoopIndexForClassify = LoopIndexForClassify;
                temp.PointIndex = PointIndex;
                temp.Type = this.Type;
                return temp;
            }
            return this;
        }


    }
}
