﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;

namespace RGeos.Framework.OTL
{
    public interface ITowerPosition
    {
       Vector3 Position { get; set; }
    }
}
