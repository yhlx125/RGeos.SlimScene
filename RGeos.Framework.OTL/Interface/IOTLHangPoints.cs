﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.Interface
{
    /// <summary>
    /// 根据回路数和挂点类型确定挂点数目和挂点名称
    /// </summary>
    public interface IOTLHangPoints
    {
        /// <summary>
        /// 回路类型
        /// </summary>
        RgLoopTypes LoopType { get; set; }
        /// <summary>
        /// 挂点类型
        /// </summary>
        RgHangPointTowerTypes TowerHangPointType { get; set; }
        /// <summary>
        /// 塔位拓扑点类型
        /// </summary>
        RgTopoPointTypes TopoPointType { get; set; }

        /// <summary>
        /// 挂点数目
        /// </summary>
        int PointCount { get; }

        IHangPoint[] HangPoints { get; set; }
        /// <summary>
        /// 用于扩展
        /// </summary>
        object Tag { get; set; }

        string Mark { get; set; }

        void SetLoopIndex(int oldIndex, int newIndex);

        //某一侧的挂点
        List<IHangPoint> GetHangPoints(int forward);

        // 获取某类型导线，某一侧的挂点
        List<IHangPoint> GetHangPoints(int hangType, int forward);
    }
}
