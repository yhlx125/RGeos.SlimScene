﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.Interface
{
    /// <summary>
    /// 双回路三相交流电
    /// </summary>
    public class AC2HangPointsFactory : IOTLHangPointsFactory
    {
        IOTLHangPoints otlHangPoints = null;

        public IOTLHangPoints OtlHangPoints
        {
            get { return otlHangPoints; }
            set { otlHangPoints = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loopType"></param>
        /// <param name="towerHangPointType"></param>
        /// <param name="topoPointType"></param>
        public void CreateOTLHangPoints(RgLoopTypes loopType, RgHangPointTowerTypes towerHangPointType, RgTopoPointTypes topoPointType)
        {
            IOTLHangPoints otlHangPoints = new OTLHangPoints();
            otlHangPoints.LoopType = loopType;
            otlHangPoints.TopoPointType = topoPointType;
            otlHangPoints.TowerHangPointType = towerHangPointType;
            if (loopType == RgLoopTypes.Two && towerHangPointType == RgHangPointTowerTypes.Z)//直线塔
            {
                IHangPoint[] mHangPts = new HangPoint[5];
                IHangPoint left = new HangPoint();
                left.Code = 0x10001010000000;
                left.Name = "左地线";
                mHangPts[0] = left;
                IHangPoint right = new HangPoint();
                right.Code = 0x10001001000000;
                right.Name = "右地线";
                mHangPts[1] = right;

                IHangPoint One = new HangPoint();
                One.Code = 0x01001000100000;
                One.Name = "回一导线一";
                mHangPts[2] = One;

                IHangPoint two = new HangPoint();
                two.Code = 0x010010000010000;
                two.Name = "回一导线二";
                mHangPts[3] = two;

                IHangPoint three = new HangPoint();
                three.Code = 0x010010000001000;
                three.Name = "回一导线三";
                mHangPts[4] = three;
                otlHangPoints.HangPoints = mHangPts;
            }
            if (loopType == RgLoopTypes.Two && towerHangPointType == RgHangPointTowerTypes.J)//转角塔
            {
                IHangPoint[] mHangPts = new HangPoint[8];
                IHangPoint left = new HangPoint();
                left.Name = "左地线";
                left.Forward = (int)RgIsForward.NotDefine;
                mHangPts[0] = left;
                IHangPoint right = new HangPoint();
                right.Name = "右地线";
                right.Forward = (int)RgIsForward.NotDefine;
                mHangPts[1] = right;

                IHangPoint One = new HangPoint();
                One.Forward = (int)RgIsForward.Front;
                One.Name = "回1导线一前侧";
                mHangPts[2] = One;

                IHangPoint two = new HangPoint();
                two.Forward = (int)RgIsForward.Front;
                two.Name = "回1导线二前侧";
                mHangPts[3] = two;

                IHangPoint three = new HangPoint();
                three.Forward = (int)RgIsForward.Front;
                three.Name = "回1导线三前侧";
                mHangPts[4] = three;

                IHangPoint OneBack = new HangPoint();
                OneBack.Forward = (int)RgIsForward.Back;
                OneBack.Name = "回1导线一后侧";
                mHangPts[5] = OneBack;

                IHangPoint twoBack = new HangPoint();
                twoBack.Forward = (int)RgIsForward.Back;
                twoBack.Name = "回1导线二后侧";
                mHangPts[6] = twoBack;

                IHangPoint threeBack = new HangPoint();
                threeBack.Forward = (int)RgIsForward.Back;
                threeBack.Name = "回1导线三后侧";
                mHangPts[7] = threeBack;
                otlHangPoints.HangPoints = mHangPts;

                IHangPoint OneJump = new HangPoint();
                OneJump.Forward = (int)RgIsForward.Middle;
                OneJump.Name = "回1跳线一";
                mHangPts[5] = OneJump;

                IHangPoint twoJump = new HangPoint();
                twoJump.Forward = (int)RgIsForward.Middle;
                twoJump.Name = "回1跳线二";
                mHangPts[6] = twoJump;

                IHangPoint threeJump = new HangPoint();
                threeJump.Forward = (int)RgIsForward.Middle;
                threeJump.Name = "回1跳线三";
                mHangPts[7] = threeJump;
                otlHangPoints.HangPoints = mHangPts;

                IHangPoint One2 = new HangPoint();
                One2.Forward = (int)RgIsForward.Front;
                One2.Name = "回2导线一前侧";
                mHangPts[2] = One2;

                IHangPoint two2 = new HangPoint();
                two2.Forward = (int)RgIsForward.Front;
                two2.Name = "回2导线二前侧";
                mHangPts[3] = two2;

                IHangPoint three2 = new HangPoint();
                three2.Forward = (int)RgIsForward.Front;
                three2.Name = "回2导线三前侧";
                mHangPts[4] = three2;

                IHangPoint OneBack2 = new HangPoint();
                OneBack2.Forward = (int)RgIsForward.Back;
                OneBack2.Name = "回2导线一后侧";
                mHangPts[5] = OneBack2;

                IHangPoint twoBack2 = new HangPoint();
                twoBack2.Forward = (int)RgIsForward.Back;
                twoBack2.Name = "回2导线二后侧";
                mHangPts[6] = twoBack2;

                IHangPoint threeBack2 = new HangPoint();
                threeBack2.Forward = (int)RgIsForward.Back;
                threeBack2.Name = "回2导线三后侧";
                mHangPts[7] = threeBack2;

                IHangPoint OneJump2 = new HangPoint();
                OneJump2.Forward = (int)RgIsForward.Middle;
                OneJump2.Name = "回2跳线一";
                mHangPts[5] = OneJump2;

                IHangPoint twoJump2 = new HangPoint();
                twoJump2.Forward = (int)RgIsForward.Middle;
                twoJump2.Name = "回2跳线二";
                mHangPts[6] = twoJump2;

                IHangPoint threeJump2 = new HangPoint();
                threeJump2.Forward = (int)RgIsForward.Middle;
                threeJump2.Name = "回2跳线三";
                mHangPts[7] = threeJump2;
                otlHangPoints.HangPoints = mHangPts;
            }
        }

        public void GetHangPointsScheme()
        {


        }

        public void InitialHangPointsValues(SlimDX.Vector3[] hangPoints)
        {
            if (otlHangPoints == null)
            {
                throw new ArgumentException("没有定义挂点信息！");
            }
            if (hangPoints.Length < otlHangPoints.PointCount)
            {
                throw new ArgumentException("挂点数目不足！");
            }
        }

        public void InitialDefaultHangPointsValues()
        {
            if (otlHangPoints == null)
            {
                throw new ArgumentException("没有定义挂点信息！");
            }
        }
    }
}
