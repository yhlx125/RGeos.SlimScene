﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.Interface
{
    /// <summary>
    /// 单回路三相交流电
    /// </summary>
    public class AC1HangPointsFactory : IOTLHangPointsFactory
    {
        IOTLHangPoints otlHangPoints = null;

        public IOTLHangPoints OtlHangPoints
        {
            get { return otlHangPoints; }
            set { otlHangPoints = value; }
        }

        private int mLoopIndex = 0;
        public void SetLoopIndex(int loopIndex)
        {
            mLoopIndex = loopIndex;
        }
        /*编码说明
         * 000-000-00000001-
         * 地线/导线/跳线-大号侧/中间/小号侧-地线1/地线2/导线1/导线2/导线3/跳1/跳2/跳3
         * */
        public void CreateOTLHangPoints(RgLoopTypes loopType, RgHangPointTowerTypes towerHangPointType, RgTopoPointTypes topoPointType)
        {
            otlHangPoints = new OTLHangPoints();
            otlHangPoints.LoopType = loopType;
            otlHangPoints.TopoPointType = topoPointType;
            otlHangPoints.TowerHangPointType = towerHangPointType;
            if (loopType == RgLoopTypes.One && towerHangPointType == RgHangPointTowerTypes.Z)//直线塔
            {
                IHangPoint[] mHangPts = new HangPoint[5];
                IHangPoint left = new HangPoint();
                left.Name = "左地线";
                left.Code = 0x10001010000000;
                left.Type = (int)RgWireStyles.WireGround;
                left.Forward = (int)RgIsForward.NotDefine;
                left.PointIndex = 0;
                mHangPts[0] = left;

                IHangPoint right = new HangPoint();
                right.Name = "右地线";
                right.Code = 0x10001001000000;
                right.Type = (int)RgWireStyles.WireGround;
                right.Forward = (int)RgIsForward.NotDefine;
                right.PointIndex = 1;
                mHangPts[1] = right;

                IHangPoint One = new HangPoint();
                One.Code = 0x01001000100000;
                One.Name = "导线一";
                One.Type = (int)RgWireStyles.Wire;
                One.Forward = (int)RgIsForward.NotDefine;
                One.PointIndex = 0;
                mHangPts[2] = One;

                IHangPoint two = new HangPoint();
                two.Code = 0x010010000010000;
                two.Name = "导线二";
                two.Type = (int)RgWireStyles.Wire;
                two.Forward = (int)RgIsForward.NotDefine;
                two.PointIndex = 1;
                mHangPts[3] = two;

                IHangPoint three = new HangPoint();
                three.Code = 0x010010000001000;
                three.Name = "导线三";
                three.Type = (int)RgWireStyles.Wire;
                three.Forward = (int)RgIsForward.NotDefine;
                three.PointIndex = 2;
                mHangPts[4] = three;
                otlHangPoints.HangPoints = mHangPts;
            }
            if (loopType == RgLoopTypes.One && towerHangPointType == RgHangPointTowerTypes.J)//转角塔
            {
                IHangPoint[] mHangPts = new HangPoint[8];
                IHangPoint left = new HangPoint();
                left.Name = "左地线";
                left.Type = (int)RgWireStyles.WireGround;
                left.Forward = (int)RgIsForward.NotDefine;
                left.PointIndex = 0;
                mHangPts[0] = left;

                IHangPoint right = new HangPoint();
                right.Name = "右地线";
                right.Type = (int)RgWireStyles.WireGround;
                right.Forward = (int)RgIsForward.NotDefine;
                right.PointIndex = 1;
                mHangPts[1] = right;

                IHangPoint One = new HangPoint();
                One.Type = (int)RgWireStyles.Wire;
                One.Forward = (int)RgIsForward.Front;
                One.PointIndex = 0;
                One.Name = "导线一前侧";
                mHangPts[2] = One;

                IHangPoint two = new HangPoint();
                two.Type = (int)RgWireStyles.Wire;
                two.Forward = (int)RgIsForward.Front;
                two.PointIndex = 1;
                two.Name = "导线二前侧";
                mHangPts[3] = two;

                IHangPoint three = new HangPoint();
                three.Type = (int)RgWireStyles.Wire;
                three.Forward = (int)RgIsForward.Front;
                three.PointIndex = 2;
                three.Name = "导线三前侧";
                mHangPts[4] = three;

                IHangPoint OneBack = new HangPoint();
                OneBack.Type = (int)RgWireStyles.Wire;
                OneBack.Forward = (int)RgIsForward.Back;
                OneBack.PointIndex = 0;
                OneBack.Name = "导线一后侧";
                mHangPts[5] = OneBack;

                IHangPoint twoBack = new HangPoint();
                twoBack.Type = (int)RgWireStyles.Wire;
                twoBack.Forward = (int)RgIsForward.Back;
                twoBack.PointIndex = 1;
                twoBack.Name = "导线二后侧";
                mHangPts[6] = twoBack;

                IHangPoint threeBack = new HangPoint();
                threeBack.Type = (int)RgWireStyles.Wire;
                threeBack.Forward = (int)RgIsForward.Back;
                threeBack.PointIndex = 2;
                threeBack.Name = "导线三后侧";
                mHangPts[7] = threeBack;

                IHangPoint OneJump = new HangPoint();
                OneJump.Type = (int)RgWireStyles.Jump;
                OneJump.Forward = (int)RgIsForward.Middle;
                OneJump.PointIndex = 0;
                OneJump.Name = "跳线一";
                mHangPts[8] = OneJump;

                IHangPoint twoJump = new HangPoint();
                twoJump.Type = (int)RgWireStyles.Jump;
                twoJump.Forward = (int)RgIsForward.Middle;
                twoJump.PointIndex = 1;
                twoJump.Name = "跳线二";
                mHangPts[9] = twoJump;

                IHangPoint threeJump = new HangPoint();
                threeJump.Type = (int)RgWireStyles.Jump;
                threeJump.Forward = (int)RgIsForward.Middle;
                threeJump.PointIndex = 2;
                threeJump.Name = "跳线三";
                mHangPts[10] = threeJump;
                otlHangPoints.HangPoints = mHangPts;
            }
        }

        public void GetHangPointsScheme()
        {


        }

        public void InitialHangPointsValues(SlimDX.Vector3[] hangPoints)
        {
            if (otlHangPoints == null)
            {
                throw new ArgumentException("没有定义挂点信息！");
            }
            if (hangPoints.Length < otlHangPoints.PointCount)
            {
                throw new ArgumentException("挂点数目不足！");
            }
        }

        public void InitialDefaultHangPointsValues()
        {
            if (otlHangPoints == null)
            {
                throw new ArgumentException("没有定义挂点信息！");
            }
            for (int i = 0; i < otlHangPoints.HangPoints.Length; i++)
            {
                if (otlHangPoints.HangPoints[i].Type == (int)RgWireStyles.WireGround)
                {
                    if (otlHangPoints.HangPoints[i].PointIndex == 0)
                    {
                        otlHangPoints.HangPoints[i].Coordinate = new SlimDX.Vector3(-13, 0, 53);
                    }
                    else
                    {
                        otlHangPoints.HangPoints[i].Coordinate = new SlimDX.Vector3(13, 0, 53);
                    }
                }
                if (otlHangPoints.HangPoints[i].Type == (int)RgWireStyles.Wire)
                {
                    if (otlHangPoints.HangPoints[i].PointIndex == 0)
                    {
                        otlHangPoints.HangPoints[i].Coordinate = new SlimDX.Vector3(-14, 0, 48);
                    }
                    else if (otlHangPoints.HangPoints[i].PointIndex == 2)
                    {
                        otlHangPoints.HangPoints[i].Coordinate = new SlimDX.Vector3(0, 0, 48);
                    }
                    else
                    {
                        otlHangPoints.HangPoints[i].Coordinate = new SlimDX.Vector3(14, 0,48);
                    }
                }
            }
        }
    }
}
