﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.Interface
{
    public class OTLHangPoints : IOTLHangPoints
    {
        private RgLoopTypes mLoopType;
        /// <summary>
        /// 回路类型
        /// </summary>
        public RgLoopTypes LoopType
        {
            get
            {
                return mLoopType;
            }
            set
            {
                mLoopType = value;
            }
        }

        private RgHangPointTowerTypes mTowerHangPointType;
        /// <summary>
        /// 杆塔挂点塔类型
        /// </summary>
        public RgHangPointTowerTypes TowerHangPointType
        {
            get
            {
                return mTowerHangPointType;
            }
            set
            {
                mTowerHangPointType = value;
            }
        }

        private RgTopoPointTypes mTopoPointType;
        /// <summary>
        /// 塔位在回路中的拓扑作用
        /// </summary>
        public RgTopoPointTypes TopoPointType
        {
            get
            {
                return mTopoPointType;
            }
            set
            {
                mTopoPointType = value;
            }
        }

        /// <summary>
        /// 挂点数目
        /// </summary>
        public int PointCount
        {
            get { return HangPoints.Length; }
        }

        private IHangPoint[] mHangPoints;
        /// <summary>
        /// 挂点数组
        /// </summary>
        public IHangPoint[] HangPoints
        {
            get
            {
                return mHangPoints;
            }
            set
            {
                mHangPoints = value;
            }
        }

        public object Tag { get; set; }

        public string Mark { get; set; }

        // 某一侧的挂点
        public List<IHangPoint> GetHangPoints(int forward)
        {
            List<IHangPoint> hangPts = new List<IHangPoint>();
            if (HangPoints.Length > 0)
            {
                for (int i = 0; i < HangPoints.Length; i++)
                {
                    if (HangPoints[i].Forward == forward)
                    {
                        hangPts.Add(HangPoints[i]);
                    }
                }
            }
            return hangPts;
        }

        /// <summary>
        /// 获取某类型导线，某一侧的挂点
        /// </summary>
        /// <param name="hangType">电力线类型</param>
        /// <param name="forward">大号侧？小号侧,-1表示选择全部</param>
        /// <returns></returns>
        public List<IHangPoint> GetHangPoints(int hangType, int forward = -1)
        {
            List<IHangPoint> hangPts = new List<IHangPoint>();
            if (HangPoints.Length > 0)
            {
                for (int i = 0; i < HangPoints.Length; i++)
                {
                    if (forward != -1)
                    {
                        if (HangPoints[i].Type == hangType && HangPoints[i].Forward == forward)
                        {
                            hangPts.Add(HangPoints[i]);
                        }
                    }
                    else
                    {
                        if (HangPoints[i].Type == hangType)
                        {
                            hangPts.Add(HangPoints[i]);
                        }
                    }

                }
            }
            return hangPts;
        }
        /// <summary>
        /// 对回路数目多的回路设置新的回路索引
        /// </summary>
        /// <param name="indxClassify"></param>
        /// <param name="newIndex"></param>
        public void SetLoopIndex(int indxClassify, int newIndex)
        {
            List<IHangPoint> hangPts = new List<IHangPoint>();
            if (HangPoints.Length > 0)
            {
                for (int i = 0; i < HangPoints.Length; i++)
                {
                    if (HangPoints[i].LoopIndexForClassify == indxClassify)
                    {
                        HangPoints[i].LoopIndex = newIndex;
                    }
                }
            }
        }
    }
}
