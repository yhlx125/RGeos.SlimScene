﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL
{
    public enum RgWireStyles
    {
        Wire = 0,//导线挂点
        WireGround = 1,//地线挂点
        Jump = 2,//跳线挂点
        other = 3 //其它
    }
}
