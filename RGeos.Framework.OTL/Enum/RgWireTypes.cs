﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL
{
    public enum RgWireTypes
    {
        Wire = 0,
        WireGround = 1,
        Jump = 2,
        other = 3
    }
}
