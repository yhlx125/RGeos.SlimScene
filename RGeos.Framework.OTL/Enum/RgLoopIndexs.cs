﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 回路的索引
    /// </summary>
    public enum RgLoopIndexs
    {
        First = 10001,
        Second = 10002,
        Third = 10003,
        Fouth = 10004,
        Fifth = 10005,
        Sixth = 10006,
        Custom = 10007
    }
}
