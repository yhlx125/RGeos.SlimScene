﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL
{
    //具有拓扑作用的塔位点
    public enum RgTopoPointTypes
    {
        //起点
        Start = 0,
        //终点
        End = 1,
        //分支点
        Branch = 2,
        //中间节点,作用不大
        Middle = 3
    }
}
