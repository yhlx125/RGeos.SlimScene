﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL
{
    //大号侧？小号侧
    public enum RgIsForward
    {
        NotDefine = -1,
        Front = 0,
        Back = 1,
        Middle = 2
    }
    public enum RgIsForward2
    {
        NotDefine = -1,
        //导线前
        WireFront = 0,
        //导线后
        WireBack = 1,
        //跳线前
        JumpFront = 2,
        //跳线后
        JumpBack = 3,

        Middle = 4
    }
}
