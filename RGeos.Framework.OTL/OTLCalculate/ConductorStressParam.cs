﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    /// <summary>
    /// 一种导线某一耐张段一种工况下的载荷值(应力参数)
    /// </summary>
    public class ConductorStressParam
    {
        public WireStyle WireStyleProperty { get; set; }

        public Condition ConditionProperty { get; set; }

        /// <summary>
        /// 综合载荷
        /// </summary>
        public double ZHZH_ConductorLoad { get; set; }
        /// <summary>
        /// 风载荷
        /// </summary>
        public double WindZH_ConductorLoad { get; set; }
        /// <summary>
        /// 垂直载荷
        /// </summary>
        public double VZH_ConductorLoad { get; set; }
        /// <summary>
        /// 导线张力
        /// </summary>
        public double ZL_WireStress { get; set; }
        /// <summary>
        /// 导线模板K值，数量级10E-5
        /// </summary>
        public double K_Value { get; set; }

        public ConductorStressParam(Condition condition)
        {
            ConditionProperty = condition;
        }
    }
}
