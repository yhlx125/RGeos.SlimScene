﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    public class WireStyle
    {
        // 导线型号
        public string EWireTypeName { get; set; }
        //E(Mpa)——弹性系数
        public double E { get; set; }
        //A(mm2)——截面积
        public double Area { get; set; }
        //α(10-6/℃)——膨胀系数
        public double Alpha { get; set; }
        //W(kg/m)——单位长度质量
        public double W { get; set; }
        //d(mm)——直径
        public double D { get; set; }
        //Tp(N)——拉断力
        public double Tp { get; set; }

        //0/1导线或地线
        public int WireType { get; set; }

        public WireStyle()
        { }
        public WireStyle(string eWireTypeName, double e, double area, double alpha, double w, double d, double tp, int wireType)
        {
            EWireTypeName = eWireTypeName;
            E = e;
            Area = area;
            Alpha = alpha;
            W = w;
            D = d;
            Tp = tp;
            WireType = wireType;
        }
    }

    public class WireTypeDAL
    {

    }
}
