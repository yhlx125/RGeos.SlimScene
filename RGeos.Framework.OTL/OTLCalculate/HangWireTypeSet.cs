﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RGeos.Framework.OTL.OTLCalculate
{
    public class HangWireTypeSet
    {
        private WireStyle eWireStyle = null;

        public WireStyle EWireStyle
        {
            get
            {
                if (eWireStyle != null)
                {
                    return eWireStyle;
                }
                else
                {
                    if (Type == (int)RgWireStyles.Wire)
                    {
                        return OtlContext.Instance().Wire;
                    }
                    else if (Type == (int)RgWireStyles.WireGround)
                    {
                        return OtlContext.Instance().Earth;
                    }
                    else if (Type == (int)RgWireStyles.Jump)
                    {
                        return OtlContext.Instance().Wire;
                    }
                    return null;
                }
            }
        }

        //导线、地线还是跳线
        public int Type { get; set; }

        //分类回路
        public int LoopIndexForClassify { get; set; }

        //所在回路,第几回路
        public int LoopIndex { get; set; }

        /**************************************
         * 挂点索引,
         **************************************/
        public int PointIndex { get; set; }

        //线综合载荷
        //public double ZHZH { get; set; }
        //线风载荷
        //public double WindZH { get; set; }
        //线垂直载荷
        // public double VZH { get; set; }
        //线张力
        //public double ZL { get; set; }
        public Color OtlPolylineColor { get; set; }

        public void SetWireType(WireStyle wireStyle)
        {
            eWireStyle = wireStyle;
        }
    }
}
