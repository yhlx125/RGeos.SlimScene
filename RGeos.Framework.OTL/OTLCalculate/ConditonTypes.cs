﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    // 最低气温 平均气温 最大风 覆冰 安装 验冰 验风 脱冰1 脱冰2 断线
    // 被冰	最大风	最低温	安装	验冰	验风	本侧脱冰1	本侧未脱冰1	本侧脱冰2	本侧未脱冰2	断线	平均气温
    public enum ConditonTypes
    {
        MinTemperature = 0,//最低气温
        AverageTempreture = 1,//平均气温
        MaxWind = 2,//大风
        Ice = 3,//覆冰
        Anzhuang = 4,//安装
        CheckIce = 5,//验冰
        CheckWind = 6,//验风
        MaxTemperature = 7,//最高气温
        BrokenLine = 8,//断线
        LostIce1 = 9,//脱冰1
        LostIce2 = 10,//脱冰2
        NotLostIce1 = 11,//本侧未脱冰1
        NotLostIce2 = 12,//本侧未脱冰2
        Other1 = 13,
        Other2 = 14,
        Other3 = 15,
        Custom = 100
    }
}
