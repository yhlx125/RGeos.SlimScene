using System;
using RGeos.Framework.OTL;
using RGeos.Geometry;
using RGeos.Basic;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 参考文献：邵天晓的《架空送电线路的电线力学计算》第二版P59
    /// </summary>
    public interface ICatenaryline
    {
        /// <summary>
        /// 计算任意一点弧垂
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        double Compute_fx(double x);
        /// <summary>
        /// 计算应力
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        double Compute_rx(double angle);
        /// <summary>
        /// 计算导线端最大应力
        /// </summary>
        /// <returns></returns>
        double Compute_ra();
        /// <summary>
        /// 计算导线端最大应力
        /// </summary>
        /// <returns></returns>
        double Compute_rb();
    }
    /// <summary>
    /// 坐标原点O点在左侧悬挂点的悬链线方程和弧垂计算
    /// </summary>
    public class OTLCatenaryline : ICatenaryline
    {
        private double mAlpha0 = 80;
        private double mGamma = 35.187 * 0.001;
        /// <summary>
        /// 传入最低点水平应力N/(m*mm2)
        /// </summary>
        public double Alpha0
        {
            get { return mAlpha0; }
            set { mAlpha0 = value; }
        }
        /// <summary>
        /// 传入电线比载N/m
        /// </summary>
        public double Gamma
        {
            get { return mGamma; }
            set { mGamma = value; }
        }
        /// <summary>
        /// 传入挂点的高差，
        /// </summary>
        public double H { get; set; }
        /// <summary>
        /// 传入档距
        /// </summary>
        public double L { get; set; }

        public OTLCatenaryline()
        {
        }

        public OTLCatenaryline(double a0, double r, double h, double l)
        {
            mAlpha0 = a0;
            mGamma = r;
            H = h;
            L = l;
        }

        #region ICatenaryline 成员
        /// <summary>
        /// 档内线长
        /// </summary>
        public double Lh0
        {
            get { return (2 * Alpha0 / Gamma) * MathEx.sh(Gamma * L / (2 * Alpha0)); }
        }
        /// <summary>
        /// 最低点距离左侧悬挂点距离
        /// </summary>
        public double a
        {
            get { return L / 2 - (Alpha0 / Gamma) * (MathEx.arsh(H / Lh0)); }
        }
        /// <summary>
        /// 最低点距离右侧悬挂点距离
        /// </summary>
        public double b
        {
            get { return L / 2 + Alpha0 / Gamma * MathEx.arsh(H / Lh0); }
        }
        /// <summary>
        /// 悬链线方程，求任意点的坐标（相对于O点）
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public virtual double Compute_y(double x)
        {
            double part1 = (H / L) * x;
            double part2 = ((Alpha0 * H) / (Gamma * Lh0)) * (MathEx.sh(Gamma * L / (2 * Alpha0)) + MathEx.sh(Gamma * (2 * x - L) / (2 * Alpha0)));
            double part3 = (2 * Alpha0 / Gamma * MathEx.sh(Gamma * x / (2 * Alpha0)) * MathEx.sh(Gamma * (L - x) / (2 * Alpha0))) * Math.Sqrt(1 + (H / Lh0) * (H / Lh0));
            return part2 - part3;
        }
        /// <summary>
        /// 计算弧垂
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public virtual double Compute_fx(double x)
        {
            double part1 = (H / L) * x;
            double part2 = ((Alpha0 * H) / (Gamma * Lh0)) * (MathEx.sh(Gamma * L / (2 * Alpha0)) + MathEx.sh(Gamma * (2 * x - L) / (2 * Alpha0)));
            double part3 = (2 * Alpha0 / Gamma * MathEx.sh(Gamma * x / (2 * Alpha0)) * MathEx.sh(Gamma * (L - x) / (2 * Alpha0))) * Math.Sqrt(1 + (H / Lh0) * (H / Lh0));
            return part1 - part2 + part3;
        }
        public virtual double Compute_fx2(double x)
        {
            double part1 = 2 * Alpha0 / Gamma;
            double part2 = MathEx.sh((Gamma * x) / (2 * Alpha0));
            double part3 = MathEx.sh(Gamma * (1 - x) / (2 * Alpha0));
            return part1 * part2 * part3;
        }
        public virtual double Compute_rx(double angle)
        {
            return Alpha0 * Math.Sqrt(1 + Math.Tan(angle) * Math.Tan(angle));
        }

        public virtual double Compute_ra()
        {
            double part1 = Alpha0;
            double part2 = Gamma * L / (2 * Alpha0);
            double part3 = 1 / MathEx.sh(H / Lh0);
            return part1 * MathEx.ch(part2 - part3);
        }

        public virtual double Compute_rb()
        {
            double part1 = Alpha0;
            double part2 = Gamma * L / (2 * Alpha0);
            double part3 = 1 / MathEx.sh(H / Lh0);
            return part1 * MathEx.ch(part2 + part3);
        }

        #endregion
    }

}
