﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;

namespace RGeos.Framework.OTL.OTLCalculate
{
    //绝缘子串
    public interface IInsulator
    {
        string Name { get; set; }
        Vector3 BlowHangPoint { get; set; }
    }
    //悬垂串
    public class HangInsulator : IInsulator
    {
        public string Name { get; set; }
        public Vector3 BlowHangPoint { get; set; }
    }

    //耐张串
    public class StrainInsulator : IInsulator
    {
        public string Name { get; set; }
        public Vector3 BlowHangPoint { get; set; }
    }
}
