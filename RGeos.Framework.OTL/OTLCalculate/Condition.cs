﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    /// <summary>
    /// 工况
    /// </summary>
    public class Condition
    {
        public string ConditionName { get; set; }

        public double Ice { get; set; }
        public double WindWire { get; set; }
        public double WindWireGround { get; set; }
        public double Tempreture { get; set; }

        public Condition()
        {
        }
        public Condition(string name)
        {
            ConditionName = name;
        }
        public virtual void Copy(Condition condi)
        {
            Ice = condi.Ice;
            WindWire = condi.WindWire;
            WindWireGround = condi.WindWireGround;
            Tempreture = condi.Tempreture;
        }
    }
   
}
