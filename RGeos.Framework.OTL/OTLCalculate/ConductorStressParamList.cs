﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    /// <summary>
    /// 某一耐张段一种导线n种不同工况
    /// </summary>
    public class ConductorStressParamList
    {
        //一种导线
        public WireStyle WireStyleProperty { get; set; }
        List<ConductorStressParam> mConditions = new List<ConductorStressParam>();
        Dictionary<string, ConductorStressParam> dic = new Dictionary<string, ConductorStressParam>();
        public int Count
        {
            get
            {
                if (mConditions != null)
                {
                    return mConditions.Count;
                }
                return 0;
            }
        }
        //索引器
        public ConductorStressParam this[int index]
        {
            get
            {
                return mConditions[index];
            }
            set
            {
                mConditions[index] = value;
            }
        }

        public ConductorStressParamList()
        {
        }

        public ConductorStressParam Get(int i)
        {
            if (mConditions != null)
            {
                return mConditions[i];
            }
            return null;
        }
        public ConductorStressParam Get(string conditionName)
        {
            if (mConditions != null)
            {
                if (dic.ContainsKey(conditionName))
                {
                    return dic[conditionName];
                }

            }
            return null;
        }
        public void Add(ConductorStressParam condi)
        {
            if (dic.ContainsKey(condi.ConditionProperty.ConditionName))
            {
            }
            else
            {
                dic.Add(condi.ConditionProperty.ConditionName, condi);
                mConditions.Add(condi);
            }
        }

    }
}
