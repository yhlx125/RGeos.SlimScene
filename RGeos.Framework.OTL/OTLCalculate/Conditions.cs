﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    public class Conditions : List<Condition>
    {
        public Conditions()
        {
        }
        public void ValidateStandard()
        {
            int flag = 0x0;
            int flag1 = 0x0;
            int flag2 = 0x0;
            int flag3 = 0x0;
            for (int i = 0; i < Count; i++)
            {
                Condition condi = this[i];
                if (condi.ConditionName == StandardCondition.Default.MinTemperature)
                {
                    flag = 0x1;
                }
                if (condi.ConditionName == StandardCondition.Default.AverageTemperature)
                {
                    flag1 = 0x1;
                }
                if (condi.ConditionName == StandardCondition.Default.IceCover)
                {
                    flag2 = 0x1;
                }
                //风载荷
                if (condi.ConditionName == StandardCondition.Default.MaxWind)
                {
                    flag3 = 0x1;
                }
            }
            if (flag == 0x0)
            {
                throw new ArgumentException(string.Format("[{0}]工况不存在", StandardCondition.Default.MinTemperature));
            }
            if (flag1 == 0x0)
            {
                throw new ArgumentException(string.Format("[{0}]工况不存在", StandardCondition.Default.AverageTemperature));
            }
            if (flag2 == 0x0)
            {
                throw new ArgumentException(string.Format("[{0}]工况不存在", StandardCondition.Default.IceCover));
            }
            if (flag3 == 0x0)
            {
                throw new ArgumentException(string.Format("[{0}]工况不存在", StandardCondition.Default.MaxWind));
            }
        }
    }
   
}
