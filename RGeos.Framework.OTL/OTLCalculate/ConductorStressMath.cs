﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGeos.Framework.OTL.OTLCalculate
{
    /// <summary>
    /// 导线应力计算
    /// 1种导线-在某1耐张段-n种工况条件下的弧垂计算
    /// </summary>
    public class ConductorStressMath
    {
        //导线型号
        private WireStyle eWireStyle = null;

        public WireStyle EWireStyle
        {
            get { return eWireStyle; }
        }

        //安全系数2.5/3.0
        private double mSafeK_Wire = 2.5;

        public double SafeK_Wire
        {
            get { return mSafeK_Wire; }
        }

        //C:20/25
        private int mC_Wire = 20;

        public int C_Wire
        {
            get { return mC_Wire; }
        }

        private double dRepresent = 300;
        /// <summary>
        ///  代表档距
        /// </summary>
        public double DRepresent
        {
            get { return dRepresent; }
        }

        private Conditions mConditions = null;
        //参与计算的工况
        public Conditions UsageConditions
        {
            get { return mConditions; }
        }

        protected ConductorStressParamList StrainConditions = null;

        //导地线型号、安全系数、代表档距参数
        public ConductorStressMath(Conditions conditions, WireStyle vWireStyle, double vSafeK_Wire, int c_wire, double vRepresent)
        {
            eWireStyle = vWireStyle;
            mSafeK_Wire = vSafeK_Wire;
            mC_Wire = c_wire;
            dRepresent = vRepresent;

            StrainConditions = new ConductorStressParamList();
            mConditions = conditions;
            for (int i = 0; i < mConditions.Count; i++)
            {
                ConductorStressParam straincond = new ConductorStressParam(mConditions[i]);
                StrainConditions.Add(straincond);
            }
        }

        //模板K值
        public double GetPolylineK(string conditionName)
        {
            ConductorStressParam condition = StrainConditions.Get(conditionName);
            ConductorStressParam huichui = condition as ConductorStressParam;
            if (huichui != null)
            {
                return huichui.K_Value;
            }
            return 0;
        }
        //综合载荷
        public double GetPolylineZHZH(string conditionName)
        {
            ConductorStressParam condition = StrainConditions.Get(conditionName);
            ConductorStressParam huichui = condition as ConductorStressParam;
            if (huichui != null)
            {
                return huichui.ZHZH_ConductorLoad;
            }
            return 0;
        }
        //垂直载荷
        public double GetPolylineVZH(string conditionName)
        {
            ConductorStressParam condition = StrainConditions.Get(conditionName);
            ConductorStressParam huichui = condition as ConductorStressParam;
            if (huichui != null)
            {
                return huichui.VZH_ConductorLoad;
            }
            return 0;
        }
        //风载荷
        public double GetPolylineWindZH(string conditionName)
        {
            ConductorStressParam condition = StrainConditions.Get(conditionName);
            ConductorStressParam huichui = condition as ConductorStressParam;
            if (huichui != null)
            {
                return huichui.WindZH_ConductorLoad;
            }
            return 0;
        }
        //导线张力
        public double GetPolylineZL(string conditionName)
        {
            ConductorStressParam condition = StrainConditions.Get(conditionName);
            ConductorStressParam huichui = condition as ConductorStressParam;
            if (huichui != null)
            {
                return huichui.ZL_WireStress;
            }
            return 0;
        }


        /// <summary>
        /// 计算张力
        /// </summary>
        public void CalculatePower()
        {
            if (StrainConditions != null && StrainConditions.Count > 0)
            {
                double[,] LJDJParamsWire = new double[4, 3];

                for (int i = 0; i < StrainConditions.Count; i++)
                {
                    ConductorStressParam condi = StrainConditions[i];
                    //导线
                    condi.ZHZH_ConductorLoad = OTLStrainCaculate.GetP(condi.ConditionProperty.WindWire, condi.ConditionProperty.Ice, eWireStyle.W, eWireStyle.D);
                    condi.VZH_ConductorLoad = OTLStrainCaculate.GetP(0, condi.ConditionProperty.Ice, eWireStyle.W, eWireStyle.D);
                    condi.WindZH_ConductorLoad = OTLStrainCaculate.GetP5(condi.ConditionProperty.WindWire, condi.ConditionProperty.Ice, eWireStyle.W, eWireStyle.D);

                    if (condi.ConditionProperty.ConditionName == StandardCondition.Default.MinTemperature)
                    {
                        LJDJParamsWire[0, 0] = condi.ConditionProperty.Tempreture;
                        LJDJParamsWire[0, 1] = condi.ZHZH_ConductorLoad;
                        LJDJParamsWire[0, 2] = eWireStyle.Tp / (double)mSafeK_Wire;
                    }
                    if (condi.ConditionProperty.ConditionName == StandardCondition.Default.AverageTemperature)
                    {
                        LJDJParamsWire[1, 0] = condi.ConditionProperty.Tempreture;
                        LJDJParamsWire[1, 1] = condi.ZHZH_ConductorLoad;
                        LJDJParamsWire[1, 2] = eWireStyle.Tp * mC_Wire / 100;
                    }
                    if (condi.ConditionProperty.ConditionName == StandardCondition.Default.IceCover)
                    {
                        LJDJParamsWire[3, 0] = condi.ConditionProperty.Tempreture;
                        LJDJParamsWire[3, 1] = condi.ZHZH_ConductorLoad;
                        LJDJParamsWire[3, 2] = eWireStyle.Tp / (double)mSafeK_Wire;

                    }
                    //风载荷
                    if (condi.ConditionProperty.ConditionName == StandardCondition.Default.MaxWind)
                    {
                        LJDJParamsWire[2, 0] = condi.ConditionProperty.Tempreture;
                        LJDJParamsWire[2, 1] = condi.ZHZH_ConductorLoad;
                        LJDJParamsWire[2, 2] = eWireStyle.Tp / (double)mSafeK_Wire;
                    }
                }
                double EA = eWireStyle.Area * eWireStyle.E;
                double EA1 = EA * eWireStyle.Alpha / Math.Pow(10, 6);
                double[,] LJDJRes = OTLStrainCaculate.LJDJ(EA, EA1, LJDJParamsWire);

                double[,] LJDJWithEAWire = new double[5, 2];
                LJDJWithEAWire[0, 0] = EA;
                LJDJWithEAWire[0, 1] = EA1;


                for (int i = 1; i < 5; i++)
                {
                    LJDJWithEAWire[i, 0] = LJDJRes[i - 1, 0];
                    LJDJWithEAWire[i, 1] = LJDJRes[i - 1, 1];
                }

                for (int i = 0; i < StrainConditions.Count; i++)
                {
                    ConductorStressParam condi = StrainConditions[i];
                    condi.ZL_WireStress = OTLStrainCaculate.GetT(dRepresent, condi.ConditionProperty.Tempreture, condi.ZHZH_ConductorLoad, LJDJWithEAWire, LJDJParamsWire);
                }
            }
        }

        public void CalculatePolylineK()
        {
            if (StrainConditions != null && StrainConditions.Count > 0)
            {
                ConductorStressParam huchui0 = StrainConditions.Get(0) as ConductorStressParam;
                if (huchui0 == null)
                {
                    throw new ArithmeticException("不是有效的导线应力参数对象");
                }
                CalculatePower();
                for (int i = 0; i < StrainConditions.Count; i++)
                {
                    ConductorStressParam huchui = StrainConditions.Get(i) as ConductorStressParam;
                    huchui.K_Value = OTLStrainCaculate.GetPolylineK(huchui.ZHZH_ConductorLoad, eWireStyle.Area, huchui.ZL_WireStress);
                }
            }
        }
    }
}
