﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.SlimScene.Core;
using RGeos.Framework.OTL.Interface;
using RGeos.Framework.OTL.Renderable;
using RGeos.SlimScene.Renderable;
using System.Windows.Forms;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 架空线路集合,渲染对象
    /// </summary>
    public class OTLPolylineList : RenderableObject
    {
        private OtlContext mContext = null;//对全局状态的引用

        private List<OtlTowerTopologicPoint> mOtlTopoPoints = null;

        private List<OTLPolyline> mPolylineList = null;

        public OTLPolylineList(string name)
            : base(name)
        {
        }
        //拓扑点表
        public virtual void InitializeTopoligicPoints()
        {
            OtlTowerTopologicPoint pt = new OtlTowerTopologicPoint();
            pt.TopoPointTypes = RgTopoPointTypes.Start;//起点
            pt.HangPointTowerType = RgHangPointTowerTypes.J;//转角塔

            OtlTowerTopologicPoint pt2 = new OtlTowerTopologicPoint();
            pt2.TopoPointTypes = RgTopoPointTypes.Branch;

            OtlTowerTopologicPoint pt3 = new OtlTowerTopologicPoint();
            pt3.TopoPointTypes = RgTopoPointTypes.End;

            mOtlTopoPoints = new List<OtlTowerTopologicPoint>();
            mOtlTopoPoints.Add(pt);
            mOtlTopoPoints.Add(pt2);
            mOtlTopoPoints.Add(pt3);
        }
        public virtual void InitializeOTLPolylineList()
        {
            //初始化示例
            mPolylineList = new List<OTLPolyline>();

            OTLPolyline ptly = new OTLPolyline();
            ptly.mLoopNum = RgLoopTypes.One;//双回路
            // ptly.AddTowerPoint(mOtlTopoPoints[0]);

            IOTLHangPointsFactory fac = new AC1HangPointsFactory();

            OtlTowerPoint otlHpt = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt.TaNumber = "T01";
            otlHpt.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt.HangPoints = fac.OtlHangPoints;
            otlHpt.Position = new SlimDX.Vector3(-300, 0, 0);
            ZModelParams modelparam = new ZModelParams();
            modelparam.FileName = string.Format(@"{0}\model\51ZBC4-51.X", Application.StartupPath);
            Tower1 tower01 = new Tower1(modelparam);
            tower01.isOn = true;
            otlHpt.TowerSymbol = tower01;
            ptly.AddTowerPoint(otlHpt);

            OtlTowerPoint otlHpt1 = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt1.TaNumber = "T02";
            otlHpt1.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt1.HangPoints = fac.OtlHangPoints;
            otlHpt1.Position = new SlimDX.Vector3(-100, 0, 0);
            // otlHpt1.TowerSymbol = new SimpleTowerModel(28, 6, 30, 5);
            ZModelParams modelparam2 = new ZModelParams();
            modelparam2.FileName = string.Format(@"{0}\model\51ZBC4-48.X", Application.StartupPath);
            Tower1 tower02 = new Tower1(modelparam2);
            tower02.isOn = true;
            otlHpt1.TowerSymbol = tower02;
            ptly.AddTowerPoint(otlHpt1);

            OtlTowerPoint otlHpt2 = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt2.TaNumber = "T03";
            otlHpt2.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt2.HangPoints = fac.OtlHangPoints;
            otlHpt2.Position = new SlimDX.Vector3(200, 0, 0);
            //otlHpt2.TowerSymbol = new SimpleTowerModel(28, 6, 30, 5);
            ZModelParams modelparam3 = new ZModelParams();
            modelparam3.FileName = string.Format(@"{0}\model\51ZBC4-45.X", Application.StartupPath);
            Tower1 tower03 = new Tower1(modelparam3);
            tower03.isOn = true;
            otlHpt2.TowerSymbol = tower03;
            ptly.AddTowerPoint(otlHpt2);

            OtlTowerPoint otlHpt3 = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt3.TaNumber = "T04";
            otlHpt3.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt3.HangPoints = fac.OtlHangPoints;
            otlHpt3.Position = new SlimDX.Vector3(300, 20, 0);
            otlHpt3.TowerSymbol = new SimpleTowerModel(28, 6, 30, 5);
            ptly.AddTowerPoint(otlHpt3);

            OtlTowerPoint otlHpt4 = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt4.TaNumber = "T05";
            otlHpt4.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt4.HangPoints = fac.OtlHangPoints;
            otlHpt4.Position = new SlimDX.Vector3(400, 20, 0);
            otlHpt4.TowerSymbol = new SimpleTowerModel(28, 6, 30, 5);
            ptly.AddTowerPoint(otlHpt4);

            OtlTowerPoint otlHpt5 = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt5.TaNumber = "T06";
            otlHpt5.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt5.HangPoints = fac.OtlHangPoints;
            otlHpt5.Position = new SlimDX.Vector3(400, 50, 0);
            otlHpt5.TowerSymbol = new SimpleTowerModel(28, 6, 30, 5);
            ptly.AddTowerPoint(otlHpt5);

            OtlTowerPoint otlHpt6 = new OtlTowerPoint();
            fac.CreateOTLHangPoints(ptly.mLoopNum, RgHangPointTowerTypes.Z, RgTopoPointTypes.Middle);
            fac.InitialDefaultHangPointsValues();
            otlHpt6.TaNumber = "T07";
            otlHpt6.HangPointTowerType = RgHangPointTowerTypes.Z;
            otlHpt6.HangPoints = fac.OtlHangPoints;
            otlHpt6.Position = new SlimDX.Vector3(400, 100, 0);
            otlHpt6.TowerSymbol = new SimpleTowerModel(28, 6, 30, 5);
            ptly.AddTowerPoint(otlHpt6);
            // ptly.AddTowerPoint(mOtlTopoPoints[1]);

            ptly.SetLoopIndex((int)RgLoopIndexs.First, 0);
            ptly.EWireStyleSet = new AC1WireStyleSet();
            ptly.InitializeStrainSection(true);

            mPolylineList.Add(ptly);
        }

        public override void Initialize(DrawArgs drawArgs)
        {
            if (mOtlTopoPoints != null)
            {
                for (int i = 0; i < this.mOtlTopoPoints.Count; i++)
                {
                    if (mOtlTopoPoints[i].IsOn && !mOtlTopoPoints[i].IsInitialize)
                    {
                        mOtlTopoPoints[i].Initialize(drawArgs);
                    }
                }
            }
            if (mPolylineList == null)
            {
                throw new ArgumentNullException("线路集合为空！");
            }
            for (int i = 0; i < mPolylineList.Count; i++)
            {
                if (mPolylineList[i].IsOn && !mPolylineList[i].IsInitialize)
                {
                    mPolylineList[i].Initialize(drawArgs);
                }

            }
            isInitialized = true;
        }
        public bool Refreshing = false;//是否刷新

        public override void Update(DrawArgs drawArgs)
        {
            if (isOn && isInitialized == false)
            {
                Initialize(drawArgs);
            }
            if (isInitialized == true && !Refreshing)
            {
                if (mOtlTopoPoints != null)
                {
                    for (int i = 0; i < this.mOtlTopoPoints.Count; i++)
                    {
                        if (mOtlTopoPoints[i].IsOn && mOtlTopoPoints[i].IsInitialize)
                        {
                            mOtlTopoPoints[i].Update(drawArgs);
                        }
                    }
                }
                if (mPolylineList != null)
                {
                    for (int i = 0; i < mPolylineList.Count; i++)
                    {
                        if (mPolylineList[i].IsOn && mPolylineList[i].IsInitialize)
                        {
                            mPolylineList[i].Update(drawArgs);
                        }
                    }
                }
            }
            if (isInitialized == true && Refreshing)
            {
                isInitialized = false;
                if (mOtlTopoPoints != null)
                {
                    for (int i = 0; i < this.mOtlTopoPoints.Count; i++)
                    {
                        if (mOtlTopoPoints[i].IsOn && mOtlTopoPoints[i].IsInitialize)
                        {
                            mOtlTopoPoints[i].Dispose();
                            mOtlTopoPoints[i].Initialize(drawArgs);
                        }
                    }
                }
                if (mPolylineList != null)
                {
                    for (int i = 0; i < mPolylineList.Count; i++)
                    {
                        if (mPolylineList[i].IsOn && mPolylineList[i].IsInitialize)
                        {
                            mPolylineList[i].Dispose();
                            mPolylineList[i].Update(drawArgs);

                        }
                    }
                }
                Refreshing = false;
            }

        }

        public override void Render(DrawArgs drawArgs)
        {
            if (!isOn)
            {
                return;
            }
            if (!isInitialized)
            {
                return;
            }
            if (mOtlTopoPoints != null)
            {
                for (int i = 0; i < this.mOtlTopoPoints.Count; i++)
                {
                    if (mOtlTopoPoints[i].IsOn && mOtlTopoPoints[i].IsInitialize)
                    {
                        mOtlTopoPoints[i].Render(drawArgs);
                    }
                }
            }
            for (int i = 0; i < mPolylineList.Count; i++)
            {
                mPolylineList[i].Render(drawArgs);
            }
        }

        public override void Dispose()
        {
            if (isInitialized == true)
            {
                for (int i = 0; i < this.mOtlTopoPoints.Count; i++)
                {
                    if (mOtlTopoPoints[i].IsOn && mOtlTopoPoints[i].IsInitialize)
                    {
                        mOtlTopoPoints[i].Dispose();
                    }
                }
                for (int i = 0; i < mPolylineList.Count; i++)
                {
                    if (mPolylineList[i].IsOn && mPolylineList[i].IsInitialize)
                    {
                        mPolylineList[i].Dispose();
                    }
                }
                isInitialized = false;
            }
        }

        public override bool PerformSelectionAction(DrawArgs drawArgs)
        {
            throw new NotImplementedException();
        }
    }

}
