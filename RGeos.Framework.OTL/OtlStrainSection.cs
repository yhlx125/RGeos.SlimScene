﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.Framework.OTL.OTLCalculate;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 单个耐张段
    /// </summary>
    public class OtlStrainSection
    {
        private List<OtlDang> mDangs;

        //代表档距
        public double Represent { get; set; }

        //导线类型
        public HangWireTypeSet[] HangWireTypeSets { get; set; }

        public OtlDang Get(int i)
        {
            if (mDangs == null)
            {
                throw new ArgumentNullException("耐张段包含的档对象为空！");
            }
            if (mDangs != null && mDangs.Count > i)
            {
                throw new ArgumentNullException("耐张段不包含该索引的档对象！");
            }
            return mDangs[i];
        }

        public void AddDang(OtlDang dang)
        {
            if (mDangs == null)
            {
                mDangs = new List<OtlDang>();
            }
            mDangs.Add(dang);
        }

        public void RemoveDang(OtlDang dang)
        {
            mDangs.Remove(dang);
        }

        // 计算代表档距
        public double GetRepresentDistance()
        {
            double represent = 0;
            double tmp = 0;
            for (int i = 0; i < mDangs.Count; i++)
            {
                tmp += mDangs[i].Distance;
                represent += Math.Pow(mDangs[i].Distance, 3);
            }
            represent = Math.Sqrt(represent / tmp);
            return represent;
        }

        public void InitialWireStyle(IWireStyleSet setss)
        {
            if (setss == null)
            {
                throw new ArgumentNullException("请设置合适的导线配置策略！");
            }
            HangWireTypeSets = setss.InitialWireStyle();
        }
    }
}
