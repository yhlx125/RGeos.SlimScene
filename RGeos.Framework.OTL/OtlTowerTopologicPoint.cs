﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using RGeos.Framework.OTL.Interface;
using RGeos.SlimScene.Core;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 塔位点
    /// </summary>
    public class OtlTowerTopologicPoint : OtlTowerPoint, IRenderable, ICloneable
    {
        public RgTopoPointTypes TopoPointTypes { get; set; }

        private List<OTLPolyline> listSurrond;

        public void AddOwnerOtlPolyline(OTLPolyline owner)
        {
            if (listSurrond == null)
            {
                listSurrond = new List<OTLPolyline>();
            }
            listSurrond.Add(owner);
            SortClockwize();
        }
        //逆时针排序
        private void SortClockwize()
        {
            throw new NotImplementedException();
        }


        void IRenderable.Initialize(DrawArgs drawArgs)
        {
            throw new NotImplementedException();
        }

        void IRenderable.Update(DrawArgs drawArgs)
        {
            throw new NotImplementedException();
        }

        void IRenderable.Render(DrawArgs drawArgs)
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }

        public object Clone()
        {
            OtlTowerTopologicPoint clone = new OtlTowerTopologicPoint();
            clone.TaNumber = TaNumber;
            clone.Position = Position;
            //clone.WireGroundHangPoints = WireGroundHangPoints;//没有什么作用，因为不通过线路的起点和终点所在档构建电力线
            return clone;
        }
    }
}
