﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGeos.Framework.OTL.OTLCalculate;

namespace RGeos.Framework.OTL
{
    /// <summary>
    /// 需要序列化对象
    /// </summary>
    public class OtlContext
    {
        public static double Resample = 0.5;

        private Condition mCurrent;

        public Condition CurrentCondition
        {
            get
            {
                if (mCurrent != null)
                {
                    return mCurrent;
                }
                else
                {
                    return mConditionList[0];
                }
            }
            set { mCurrent = value; }
        }
        private Conditions mConditionList = new Conditions();
        private static OtlContext mContext = null;
        public static OtlContext Instance()
        {
            if (mContext == null)
            {
                mContext = new OtlContext();
            }
            return mContext;
        }
        private WireStyle mEarth = null;
        public WireStyle Earth
        {
            get
            {
                if (mEarth == null)
                {
                    throw new ArgumentNullException("设计输入的导线类型为空！");
                }
                return mEarth;
            }
            set
            {
                mEarth = value;
            }
        }
        private WireStyle mWire = null;
        public WireStyle Wire
        {
            get
            {
                if (mWire == null)
                {
                    throw new ArgumentNullException("设计输入的导线类型为空！");
                }
                return mWire;
            }
            set
            {
                mWire = value;
            }
        }

        public Conditions ConditionList
        {
            get { return mConditionList; }
        }
        private OtlContext()
        {
            /*工况	    温度	冰厚	风速（导线）	风速（地线）
              最低温	-20	0	0	0
              平均气温	15	0	0	0
              最大风	-5	0	41.3	41.7
              被冰	-5	15	13.8	13.9
              */
            Condition cond1 = new Condition("最低气温");
            cond1.Tempreture = -20;
            cond1.Ice = 0;
            cond1.WindWire = 0;
            cond1.WindWireGround = 0;
            mConditionList.Add(cond1);
            Condition cond2 = new Condition("平均气温");
            cond2.Tempreture = 15;
            cond2.Ice = 0;
            cond2.WindWire = 0;
            cond2.WindWireGround = 0;
            mConditionList.Add(cond2);
            Condition cond3 = new Condition("最大风");
            cond3.Tempreture = -5;
            cond3.Ice = 0;
            cond3.WindWire = 41.3;
            cond3.WindWireGround = 41.7;
            mConditionList.Add(cond3);
            Condition cond4 = new Condition("覆冰");
            cond4.Tempreture = -5;
            cond4.Ice = 15;
            cond4.WindWire = 13.8;
            cond4.WindWireGround = 13.9;
            mConditionList.Add(cond4);
            mConditionList.ValidateStandard();
        }
        public void InitialConditions()
        {

        }
    }
}
